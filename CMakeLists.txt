cmake_minimum_required(VERSION 3.29)

# CMake requires top-level CMakeLists.txt to have project() call.
# This suggests that my approach is not ideal
# TODO: investigate if it can be done better
project(CAN-Node)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

option(BUILD_DOCS "Build documentation" OFF)
option(FORCE_COLORED_OUTPUT "Always produce ANSI-colored output (GNU/Clang only)." ON)
set(PLATFORM_TARGET "KE06" CACHE STRING "What platform to build the firmware for (KE06/TEST)")

configure_file("${CMAKE_HOME_DIRECTORY}/utils/post-commit.in"
               "${CMAKE_HOME_DIRECTORY}/.git/hooks/post-commit"
               FILE_PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE
)

if(BUILD_DOCS)
    add_subdirectory(docs)
    # TODO
endif()

if (${FORCE_COLORED_OUTPUT})
    if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
       add_compile_options (-fdiagnostics-color=always)
    elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
       add_compile_options (-fcolor-diagnostics)
    endif ()
endif ()

if (PLATFORM_TARGET STREQUAL "TEST")
    add_subdirectory(tests binary)
else()
    add_subdirectory(src binary)
endif()

file(GLOB_RECURSE SOURCES_TO_FORMAT *.cpp *.hpp)
add_custom_target(format
    COMMENT "Formatting codebase.."
    COMMAND clang-format -i ${SOURCES_TO_FORMAT}
)

