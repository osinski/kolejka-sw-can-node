/*
 * i2c.hpp
 * Copyright (C) 2024 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#pragma once

#include <cstdint>
#include <optional>
#include <span>

namespace BSP::I2C_Bus {

/**
 * @brief Enum with definitions of errors that are possible to appear during I2C communication.
 */
enum class Error
{
    BUSY,
    NAK,
    TIMEOUT,
    ARBITRATION_LOST,
};

/**
 * @class I2C
 *
 * @brief Wrapper class on FSL I2C driver.
 *
 * @note  This wrapper is not feature complete, e.g. it only supports master mode
 *
 */
template<typename PeriphType>
class I2CMasterDriver
{
  public:
    /**
     * @brief Constructor.
     *
     * @param i2cInstance Pointer to base of I2C module instance.
     * @param busFrequency Frequency that bus will operate with (SCL frequency).
     */
    I2CMasterDriver(PeriphType* i2c, uint32_t busFrequency);

    /**
     * @brief I2C peripheral configuration function.
     */
    void configure() const;

    /**
     * @brief Generates START condition on the bus and sends address of the slave
     *        device with R/W bit set to 0 (Write).
     *
     * @details  Automatically generates STOP condition when bus error (e.g. slave
     *           address NACK) is encountered.
     *
     * @param slaveAddr Address of the slave device.
     *
     * @return Optional error code.
     * @retval None Empty optional if no error occured.
     * @retval Error Error enum class value indicating what kind of error occured.
     */
    [[nodiscard]] std::optional<Error> masterStartWrite(uint8_t slaveAddr) const;

    /**
     * @brief Generates REPEATED START condition on the bus and sends address
     *        of the slave device with R/W bit set to 1 (Read).
     *
     * @details  Automatically generates STOP condition when bus error (e.g. slave
     *           address NACK) is encountered.
     *
     * @param slaveAddr Address of the slave device.
     *
     * @return Optional error code.
     * @retval None Empty optional if no error occured.
     * @retval Error Error enum class value indicating what kind of error occured.
     */
    [[nodiscard]] std::optional<Error> masterRepeatStartRead(uint8_t slaveAddr) const;

    /**
     * @brief Writes single byte on the bus. Possible use-case is to send subaddress (e.g. device
     *        register address) after REPEATED START condition was generated and slave address
     *        was sent using `masterRepeatStartRead` function.
     *
     * @details  Automatically generates STOP condition when bus error (e.g. slave
     *           address NACK) is encountered.
     *
     * @param byte Byte to be put on the bus, e.g. device register address (subaddress).
     *
     * @return Optional error code.
     * @retval None Empty optional if no error occured.
     * @retval Error Error enum class value indicating what kind of error occured.
     */
    [[nodiscard]] std::optional<Error> masterWriteSubaddress(uint8_t byte) const;

    /**
     * @brief Puts N Bytes on the bus. Generates STOP condition after last byte is sent.
     *
     * @param writeBuf Buffer with data to be put into the bus.
     *
     * @return Optional error code.
     * @retval None Empty optional if no error occured.
     * @retval Error Error enum class value indicating what kind of error occured.
     */
    [[nodiscard]] std::optional<Error> masterWriteAndStop(std::span<uint8_t> writeBuf) const;

    /**
     * @brief Reads N Bytes from the bus. Generates STOP condition after last byte is sent.
     *
     * @param readBuf Buffer for data to be read from the bus.
     *
     * @return Optional error code.
     * @retval None Empty optional if no error occured.
     * @retval Error Error enum class value indicating what kind of error occured.
     */
    [[nodiscard]] std::optional<Error> masterReadAndStop(std::span<uint8_t> readBuf) const;

    /**
     * @brief Generates STOP condition on the bus.
     */
    void masterStop() const;

    I2CMasterDriver() = delete;
    I2CMasterDriver(const I2CMasterDriver&) = delete;
    I2CMasterDriver(I2CMasterDriver&&) = delete;
    I2CMasterDriver operator=(const I2CMasterDriver&) = delete;
    I2CMasterDriver operator=(I2CMasterDriver&&) = delete;
    ~I2CMasterDriver() = default;

  private:
    PeriphType* i2cBase;
    uint32_t freq;

    void waitForTransferCompleteFlag() const;
    void waitForInterruptFlag() const;
    void clearInterruptFlag() const;
    void clearStatusReg() const;
    void clearStartStopFlags() const;
    [[nodiscard]] std::optional<Error> checkForError() const;
};

}
