/*
 * uart.cpp
 * Copyright (C) 2024 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "uart.hpp"
#include "vendor/MKE06Z4.h"
#include "vendor/sdk/fsl_uart.h"

template<>
BSP::UART<UART_Type>::UART(UART_Type* uart, uint32_t baudrate)
  : uartBase{ uart }
  , baudrate{ baudrate }
{
}

template<>
void
BSP::UART<UART_Type>::handleTransmissionCompleted()
{
    this->transmitting = false;
    this->uartBase->C2 &= ~(UART_C2_TCIE_MASK | UART_C2_TIE_MASK);
}

template<>
void
BSP::UART<UART_Type>::configure()
{
    static uart_config_t uartCfg = { .baudRate_Bps = baudrate,
                                     .parityMode = kUART_ParityDisabled,
                                     .stopBitCount = kUART_OneStopBit,
                                     .idleType = kUART_IdleTypeStopBit,
                                     .enableTx = true,
                                     .enableRx = true };
    UART_Init(this->uartBase, &uartCfg, CLOCK_GetBusClkFreq());
    UART_EnableInterrupts(this->uartBase, kUART_RxDataRegFullInterruptEnable);
}

template<>
void
BSP::UART<UART_Type>::writeChar(char c)
{
    this->uartBase->D = c;
}

template<>
char
BSP::UART<UART_Type>::readChar()
{
    return this->uartBase->D;
}

template<>
bool
BSP::UART<UART_Type>::send(const char* string)
{
    if (this->transmitting || string == nullptr) {
        return false;
    }
    this->transmitting = true;
    strncpy(this->txBuf.data(), string, this->txBuf.max_size());
    this->posToWriteFrom = 0;

    this->uartBase->C2 |= UART_C2_TIE_MASK;

    return true;
}

template<>
void
BSP::UART<UART_Type>::handleCharacterSent()
{
    if (this->transmitting) {
        const char nextChar = this->txBuf[posToWriteFrom];
        if (nextChar != '\0' && (this->posToWriteFrom < this->txBuf.size())) {
            ++(this->posToWriteFrom);
            writeChar(nextChar);
        } else {
            handleTransmissionCompleted();
        }
    }
}

template<>
[[nodiscard]] bool
BSP::UART<UART_Type>::isTransmitting() const
{
    return this->transmitting;
}
