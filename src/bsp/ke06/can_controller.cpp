/*
 * can_comm.cpp
 * Copyright (C) 2024 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "bsp/can_controller.hpp"
#include "vendor/MKE06Z4.h"
#include "vendor/sdk/fsl_mscan.h"

template<>
BSP::CAN_Ctrl::ControllerDriver<MSCAN_Type>::ControllerDriver(MSCAN_Type* controller,
                                                              BSP::CAN_Ctrl::BusSpeed busspeed)
  : controllerBase{ controller }
  , speed{ busspeed }
{
}

template<>
void
BSP::CAN_Ctrl::ControllerDriver<MSCAN_Type>::configure(Filter filter, OperationMode mode) const
{
    mscan_config_t mscanConfig;
    MSCAN_GetDefaultConfig(&mscanConfig);
    mscanConfig.clkSrc = kMSCAN_ClkSrcBus;
    mscanConfig.baudRate = static_cast<uint32_t>(speed);
    mscanConfig.filterConfig.filterMode = kMSCAN_Filter32Bit;
    mscanConfig.filterConfig.u32IDAR0 = filter.acceptanceValue;
    mscanConfig.filterConfig.u32IDMR0 = filter.maskValue;
    mscanConfig.filterConfig.u32IDAR1 = filter.acceptanceValue;
    mscanConfig.filterConfig.u32IDMR1 = filter.maskValue;

    if (mode == BSP::CAN_Ctrl::OperationMode::LISTEN_ONLY) {
        mscanConfig.enableListen = true;
    } else if (mode == BSP::CAN_Ctrl::OperationMode::LOOPBACK) {
        mscanConfig.enableLoopBack = true;
    }

    MSCAN_Init(this->controllerBase, &mscanConfig, CLOCK_GetBusClkFreq());

    MSCAN_EnableRxInterrupts(this->controllerBase, kMSCAN_RxFullInterruptEnable);
    EnableIRQ(MSCAN_1_IRQn);
}

template<>
void
BSP::CAN_Ctrl::ControllerDriver<MSCAN_Type>::sendFrame(
  const BSP::CAN_Ctrl::Frame& frameToSend) const
{
    static mscan_frame_t frame{};
    frame.ID_Type.ID = frameToSend.id;
    frame.format = kMSCAN_FrameFormatStandard;
    frame.type = kMSCAN_FrameTypeData;

    frame.DLR = frameToSend.data.size();

    memcpy(frame.DSR, frameToSend.data.data(), frameToSend.data.size());

    MSCAN_TransferSendBlocking(controllerBase, &frame);
}

template<>
[[nodiscard]] BSP::CAN_Ctrl::Frame
BSP::CAN_Ctrl::ControllerDriver<MSCAN_Type>::readReceivedFrame() const
{
    mscan_frame_t mscanFrame{};
    BSP::CAN_Ctrl::Frame frame{};

    MSCAN_ReadRxMb(controllerBase, &mscanFrame);
    MSCAN_ClearRxBufferFullFlag(controllerBase);

    memcpy(frame.data.data(), mscanFrame.DSR, mscanFrame.DLR);

    frame.id = mscanFrame.ID_Type.ID;

    return frame;
}
