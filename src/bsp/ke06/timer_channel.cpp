/*
 * timer_channel.cpp
 * Copyright (C) 2024 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "timer_channel.hpp"
#include "vendor/MKE06Z4.h"
#include "vendor/sdk/fsl_ftm.h"

template<>
BSP::TimerChannel::OutputCompareDriver<FTM_Type>::OutputCompareDriver(
  FTM_Type* timer,
  uint8_t chnl,
  BSP::TimerChannel::Polarity pol)
  : timBase{ timer }
  , timChnl{ chnl }
  , timChnlPolarity{ pol }
{
}

template<>
void
BSP::TimerChannel::OutputCompareDriver<FTM_Type>::configure()
{
    static constexpr ftm_clock_prescale_t prescaleVal = kFTM_Prescale_Divide_128;
    //  NOTE: clock enabled in BSP::CPU::configureClock()
    this->prescaledFreq = CLOCK_GetTimerClkFreq() / (1U << prescaleVal);

    // Status and Control
    this->timBase->SC = FTM_SC_TOIE(0) |        // Timer Overflow Interrupt Enable (disabled)
                        FTM_SC_CPWMS(0) |       // Center-Aligned PWM Select (Up Counting Mode)
                        FTM_SC_CLKS(0) |        // Clock Source Selection (none, counter disabled)
                        FTM_SC_PS(prescaleVal); // Prescaler Selection
    // Modulo
    this->timBase->MOD = this->prescaledFreq - 1;

    if (this->timChnlPolarity == BSP::TimerChannel::Polarity::LOW) {
        // NOTE: 0 = active high, 1 = active low
        this->timBase->POL |= 1 << this->timChnl;
        // NOTE: since polarity is low should have logic high at start
        this->timBase->OUTINIT |= 1 << this->timChnl;
    }
    // NOTE: else = registers reset values are ok

    // Features Mode Selection
    this->timBase->MODE = FTM_MODE_FAULTIE(0) | // Fault Interrupt Enable (disabled)
                          FTM_MODE_FAULTM(0) |  // Fault Control Mode (disabled for all channels)
                          FTM_MODE_CAPTEST(0) | // Capture Test Mode Enable (disabled)
                          FTM_MODE_PWMSYNC(0) | // PWM Sync Mode (no restrictions)
                          FTM_MODE_WPDIS(1) |   // Write Protection Disable (protection disabled)
                          FTM_MODE_INIT(1) |    // Init Channel Outputs (enabled / initialize)
                          FTM_MODE_FTMEN(1);    // FTM Enable (?)

    FTM_SetupOutputCompare(this->timBase,
                           static_cast<ftm_chnl_t>(this->timChnl),
                           kFTM_ClearOnMatch,
                           this->prescaledFreq - 1);
    FTM_SetSoftwareTrigger(this->timBase, true);
    FTM_StartTimer(this->timBase, kFTM_SystemClock); // SystemClock in this case is 'Timer Clock'
}

template<>
void
BSP::TimerChannel::OutputCompareDriver<FTM_Type>::swControlEnable()
{
    this->timBase->SWOCTRL |= (1UL << this->timChnl);
}

template<>
void
BSP::TimerChannel::OutputCompareDriver<FTM_Type>::swControlDisable()
{
    this->timBase->SWOCTRL &= ~(1UL << this->timChnl);
}

template<>
void
BSP::TimerChannel::OutputCompareDriver<FTM_Type>::swControlSetActive()
{
    this->timBase->SWOCTRL |= (1UL << (this->timChnl + FTM_SWOCTRL_CH0OCV_SHIFT));
}

template<>
void
BSP::TimerChannel::OutputCompareDriver<FTM_Type>::swControlSetInactive()
{
    this->timBase->SWOCTRL &= ~(1UL << (this->timChnl + FTM_SWOCTRL_CH0OCV_SHIFT));
}

template<>
void
BSP::TimerChannel::OutputCompareDriver<FTM_Type>::swControlToggle()
{
    this->timBase->SWOCTRL ^= (1UL << (this->timChnl + FTM_SWOCTRL_CH0OCV_SHIFT));
}

template<>
void
BSP::TimerChannel::OutputCompareDriver<FTM_Type>::startOutputCompare()
{
    FTM_SetupOutputCompare(this->timBase,
                           static_cast<ftm_chnl_t>(this->timChnl),
                           kFTM_ToggleOnMatch,
                           this->prescaledFreq - 1);
    FTM_StartTimer(this->timBase, kFTM_SystemClock);
}

template<>
void
BSP::TimerChannel::OutputCompareDriver<FTM_Type>::stopOutputCompare()
{
    FTM_StopTimer(this->timBase);
    FTM_SetupOutputCompare(this->timBase,
                           static_cast<ftm_chnl_t>(this->timChnl),
                           kFTM_ClearOnMatch,
                           this->prescaledFreq - 1);
}
