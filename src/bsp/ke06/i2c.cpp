/*
 * i2c.cpp
 * Copyright (C) 2024 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "i2c.hpp"
#include "vendor/MKE06Z4.h"
#include "vendor/sdk/fsl_i2c.h"

template<>
BSP::I2C_Bus::I2CMasterDriver<I2C_Type>::I2CMasterDriver(I2C_Type* i2c, uint32_t busFrequency)
  : i2cBase{ i2c }
  , freq{ busFrequency } {};

template<>
void
BSP::I2C_Bus::I2CMasterDriver<I2C_Type>::configure() const
{
    i2c_master_config_t cfg;

    I2C_MasterGetDefaultConfig(&cfg);
    cfg.baudRate_Bps = this->freq;

    I2C_MasterInit(this->i2cBase, &cfg, CLOCK_GetBusClkFreq());
}

template<>
void
BSP::I2C_Bus::I2CMasterDriver<I2C_Type>::masterStop() const
{
    I2C_MasterStop(this->i2cBase);
}

template<>
void
BSP::I2C_Bus::I2CMasterDriver<I2C_Type>::waitForTransferCompleteFlag() const
{
    while ((this->i2cBase->S & I2C_S_TCF_MASK) == 0x00) {
    }
}

template<>
void
BSP::I2C_Bus::I2CMasterDriver<I2C_Type>::waitForInterruptFlag() const
{
    while ((this->i2cBase->S & I2C_S_IICIF_MASK) == 0x00) {
    }
}

template<>
void
BSP::I2C_Bus::I2CMasterDriver<I2C_Type>::clearStatusReg() const
{
    this->i2cBase->S = 0xFF;
}

template<>
void
BSP::I2C_Bus::I2CMasterDriver<I2C_Type>::clearStartStopFlags() const
{
    this->i2cBase->FLT |= (I2C_FLT_STARTF_MASK | I2C_FLT_STOPF_MASK);
}

template<>
std::optional<BSP::I2C_Bus::Error>
BSP::I2C_Bus::I2CMasterDriver<I2C_Type>::checkForError() const
{
    const uint8_t status = this->i2cBase->S;
    const uint8_t control = this->i2cBase->C1;

    if ((status & I2C_S_ARBL_MASK) != 0U) {
        return BSP::I2C_Bus::Error::ARBITRATION_LOST;
    }
    if (((status & I2C_S_BUSY_MASK) != 0U) && ((control & I2C_C1_MST_MASK) == 0U)) {
        return BSP::I2C_Bus::Error::BUSY;
    }
    if ((status & I2C_S_RXAK_MASK) != 0U) {
        return BSP::I2C_Bus::Error::NAK;
    }
    return std::nullopt;
}

template<>
std::optional<BSP::I2C_Bus::Error>
BSP::I2C_Bus::I2CMasterDriver<I2C_Type>::masterStartWrite(uint8_t slaveAddr) const
{
    clearStatusReg();
    clearStartStopFlags();
    waitForTransferCompleteFlag();
    if (kStatus_I2C_Busy == I2C_MasterStart(this->i2cBase, slaveAddr, kI2C_Write)) {
        return BSP::I2C_Bus::Error::BUSY;
    }
    waitForInterruptFlag();
    std::optional<BSP::I2C_Bus::Error> err = checkForError();
    if (err) {
        masterStop();
        return err;
    }
    clearStatusReg();
    return std::nullopt;
}

template<>
std::optional<BSP::I2C_Bus::Error>
BSP::I2C_Bus::I2CMasterDriver<I2C_Type>::masterRepeatStartRead(uint8_t slaveAddr) const
{
    clearStatusReg();
    clearStartStopFlags();
    waitForTransferCompleteFlag();
    if (kStatus_I2C_Busy == I2C_MasterRepeatedStart(this->i2cBase, slaveAddr, kI2C_Read)) {
        return BSP::I2C_Bus::Error::BUSY;
    }
    waitForInterruptFlag();
    std::optional<BSP::I2C_Bus::Error> err = checkForError();
    if (err) {
        masterStop();
        return err;
    }
    clearStatusReg();
    return std::nullopt;
}

template<>
std::optional<BSP::I2C_Bus::Error>
BSP::I2C_Bus::I2CMasterDriver<I2C_Type>::masterWriteSubaddress(uint8_t subAddr) const
{
    this->i2cBase->D = subAddr;
    waitForInterruptFlag();
    std::optional<BSP::I2C_Bus::Error> err = checkForError();
    if (err) {
        masterStop();
        return err;
    }
    clearStatusReg();
    return std::nullopt;
}

template<>
void
BSP::I2C_Bus::I2CMasterDriver<I2C_Type>::clearInterruptFlag() const
{
    this->i2cBase->S = I2C_S_IICIF_MASK;
}

template<>
std::optional<BSP::I2C_Bus::Error>
BSP::I2C_Bus::I2CMasterDriver<I2C_Type>::masterWriteAndStop(std::span<uint8_t> writeBuf) const
{
    const status_t result = I2C_MasterWriteBlocking(
      this->i2cBase, writeBuf.data(), writeBuf.size(), kI2C_TransferDefaultFlag);

    if (result == kStatus_I2C_Nak) {
        return BSP::I2C_Bus::Error::NAK;
    }
    if (result == kStatus_I2C_ArbitrationLost) {
        return BSP::I2C_Bus::Error::ARBITRATION_LOST;
    }
    return std::nullopt;
}

template<>
std::optional<BSP::I2C_Bus::Error>
BSP::I2C_Bus::I2CMasterDriver<I2C_Type>::masterReadAndStop(std::span<uint8_t> readBuf) const
{
    const status_t result = I2C_MasterReadBlocking(
      this->i2cBase, readBuf.data(), readBuf.size(), kI2C_TransferDefaultFlag);

    if (result == kStatus_I2C_Timeout) {
        return BSP::I2C_Bus::Error::TIMEOUT;
    }
    return std::nullopt;
}
