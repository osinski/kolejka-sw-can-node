/*
 * gpio.cpp
 * Copyright (C) 2024 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "gpio.hpp"
#include "vendor/MKE06Z4.h"
#include "vendor/sdk/fsl_port.h"

template<>
BSP::GPIO::PinInput<GPIO_Type>::PinInput(GPIO_Type* port, uint8_t pin)
  : portBase{ port }
  , pin{ pin }
{
    // TODO assert that pin is 0..31
}

template<>
void
BSP::GPIO::PinInput<GPIO_Type>::configure()
{
    this->portBase->PDDR &= ~(1U << pin);
    this->portBase->PIDR &= ~(1U << pin);
}

template<>
[[nodiscard]] BSP::GPIO::LogicState
BSP::GPIO::PinInput<GPIO_Type>::read() const
{
    const auto state = (this->portBase->PDIR >> this->pin) & 0x00000001U;

    return static_cast<BSP::GPIO::LogicState>(state);
}

template<>
BSP::GPIO::PinOutput<GPIO_Type>::PinOutput(GPIO_Type* port, uint8_t pin)
  : portBase{ port }
  , pin{ pin }
{
    // TODO assert that pin is 0..31
}

template<>
void
BSP::GPIO::PinOutput<GPIO_Type>::configure()
{
    this->portBase->PDDR |= (1U << pin);
    this->portBase->PIDR |= (1U << pin);
    this->portBase->PCOR |= (1U << pin);
}

template<>
void
BSP::GPIO::PinOutput<GPIO_Type>::set()
{
    this->portBase->PSOR |= (1U << pin);
}

template<>
void
BSP::GPIO::PinOutput<GPIO_Type>::clear()
{
    this->portBase->PCOR |= (1U << pin);
}

template<>
void
BSP::GPIO::PinOutput<GPIO_Type>::toggle()
{
    this->portBase->PTOR |= (1U << pin);
}

template<>
void
BSP::GPIO::PinOutput<GPIO_Type>::write(BSP::GPIO::LogicState state)
{
    if (state == BSP::GPIO::LogicState::LOW) {
        this->clear();
    } else {
        this->set();
    }
}

template<>
BSP::GPIO::Port<GPIO_Type>::Port(GPIO_Type* port)
  : portBase{ port }
{
}

template<>
void
BSP::GPIO::Port<GPIO_Type>::configureDirections(uint32_t dirMask) const
{
    this->portBase->PDOR = 0x00;
    this->portBase->PDDR = dirMask;
    this->portBase->PIDR = dirMask;
}

template<>
uint32_t
BSP::GPIO::Port<GPIO_Type>::inputRegisterRead() const
{
    return this->portBase->PDIR;
}

template<>
void
BSP::GPIO::Port<GPIO_Type>::outputRegisterWrite(uint32_t newValues) const
{
    this->portBase->PDOR = newValues;
}

void
BSP::GPIO::portControlConfigure()
{
    // set pinmuxes
    PORT_SetPinSelect(kPORT_I2C0, kPORT_I2C0_SCLPTA3_SDAPTA2);
    PORT_SetPinSelect(kPORT_I2C1, kPORT_I2C1_SCLPTE1_SDAPTE0);
    PORT_SetPinSelect(kPORT_MSCAN, kPORT_MSCAN_TXPTC7_RXPTC6);
    PORT_SetPinSelect(kPORT_UART0, kPORT_UART0_RXPTB0_TXPTB1);
    PORT_SetPinSelect(kPORT_FTM2CH0, kPORT_FTM2_CH0_PTC0);
    PORT_SetPinSelect(kPORT_FTM2CH1, kPORT_FTM2_CH1_PTC1);
    PORT_SetPinSelect(kPORT_FTM2CH2, kPORT_FTM2_CH2_PTC2);
    // disable all Pull-Ups

    // set input filter
    static constexpr auto settingFLTDIV3 = 0x7; // LPOCLK/128
    static constexpr auto settingFLTx = 0x3;    // FLTDIV3
    PORT->IOFLT0 = PORT_IOFLT0_FLTDIV3(settingFLTDIV3) | PORT_IOFLT0_FLTH(settingFLTx) |
                   PORT_IOFLT0_FLTG(settingFLTx) | PORT_IOFLT0_FLTF(settingFLTx) |
                   PORT_IOFLT0_FLTE(settingFLTx) | PORT_IOFLT0_FLTD(settingFLTx) |
                   PORT_IOFLT0_FLTC(settingFLTx) | PORT_IOFLT0_FLTB(settingFLTx) |
                   PORT_IOFLT0_FLTA(settingFLTx);
}
