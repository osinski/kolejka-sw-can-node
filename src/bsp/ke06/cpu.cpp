/*
 * cpu.cpp
 * Copyright (C) 2024 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "cpu.hpp"
#include "fsl_clock.h"

bool
BSP::CPU::configureClock()
{
    const osc_config_t confOSC = {
        .freq = 8'000'000,
        .workMode = kOSC_ModeOscLowPower,
        .enableMode = kOSC_Enable | kOSC_EnableInStop,
    };
    const ics_config_t confICS = {
        .icsMode = kICS_ModeFBE, // FLL Bypassed External
        .irClkEnableMode = 0,    // internal RC clock disabled
        .rDiv = 3,               // RDIV is between OSCCLK and input to FLL
                                 // If OSC is in High Freq Mode (4-25MHz)
                                 // FLL input is OSCCLK divided by 2^(5+RDIV)
                                 // FLL input frequency has to be between
                                 // 31250Hz and 390625Hz
        .bDiv = 0,               // ICS_OUT_CLK is divided by 2^BDIV
    };
    const sim_clock_config_t confSIMClk = {
        // DIV1 is source for Core and System clocks and DIV2
        .outDiv1 = 0, // divides by 2^(outDiv1) (max val is 3)
        // DIV2 is source for Flash and Bus slaves and peripherals
        .outDiv2 = 0, // divides by 2^(outDiv2) (max val is 1)
        // DIV3 is source for FTM and PWT timers
        .outDiv3 = 1,        // divides by 2^(outDiv3) (max val is 1)
        .busClkPrescaler = 0 // divides by 2^(busClkPrescaler)
    };

    CLOCK_SetSimSafeDivs();

    CLOCK_InitOsc0(&confOSC);
    CLOCK_SetXtal0Freq(confOSC.freq);

    const status_t status = CLOCK_SetIcsConfig(&confICS);
    if (status != kStatus_Success) {
        return false;
    }

    CLOCK_SetSimConfig(&confSIMClk);

    SystemCoreClockUpdate();

    // Clock Gates are individually enabled by fsl drivers for given module
    // These are done here explicitly for clarity and in case some module
    // is used without its fsl driver.
    CLOCK_EnableClock(kCLOCK_Ftm2);
    CLOCK_EnableClock(kCLOCK_I2c0);
    CLOCK_EnableClock(kCLOCK_I2c1);
    CLOCK_EnableClock(kCLOCK_Uart0);
    CLOCK_EnableClock(kCLOCK_Mscan0);

    return true;
}

bool
BSP::CPU::configurePower()
{
    // KE06 has very simple power control. Low Voltage Detection capabilities
    // are provided.
    // Application doesn't require low power operation

    return true;
}

bool
BSP::CPU::configureWatchdog()
{
    // TODO

    return true;
}

bool
BSP::CPU::configureSysTick()
{
    return SysTick_Config(SystemCoreClock) == 0;
}

bool
BSP::CPU::configureInterrupts()
{
    status_t status = EnableIRQ(SysTick_IRQn);
    if (status != kStatus_Success) {
        return false;
    }

    status = EnableIRQ(MSCAN_1_IRQn);
    if (status != kStatus_Success) {
        return false;
    }

    status = EnableIRQ(UART0_IRQn);
    return status == kStatus_Success;
}
