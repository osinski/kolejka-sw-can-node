/*
 * timer_channel.hpp
 * Copyright (C) 2024 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#pragma once

#include <cstdint>
#include <type_traits>

namespace BSP::TimerChannel {

enum class Polarity
{
    LOW,
    HIGH,
};
static_assert(static_cast<std::underlying_type_t<Polarity>>(Polarity::LOW) == 0);
static_assert(static_cast<std::underlying_type_t<Polarity>>(Polarity::HIGH) == 1);

// TODO: come up with better API
// also split this into spearate counter + channel code/classes?
template<typename PeriphType>
class OutputCompareDriver
{
  public:
    OutputCompareDriver(PeriphType* timer, uint8_t chnl, Polarity pol);
    OutputCompareDriver(OutputCompareDriver&&) = default;

    void configure();

    void swControlEnable();
    void swControlDisable();
    void swControlSetActive();
    void swControlSetInactive();
    void swControlToggle();

    void startOutputCompare();
    void stopOutputCompare();

    OutputCompareDriver() = delete;
    OutputCompareDriver(const OutputCompareDriver&) = delete;
    OutputCompareDriver operator=(const OutputCompareDriver&) = delete;
    OutputCompareDriver operator=(OutputCompareDriver&&) = delete;
    ~OutputCompareDriver() = default;

  private:
    PeriphType* timBase;
    uint8_t timChnl;
    Polarity timChnlPolarity;
    uint32_t prescaledFreq = 0;
};

}
