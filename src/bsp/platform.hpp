/*
 * patform.hpp
 * Copyright (C) 2024 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#pragma once

#if defined(PLATFORM_KE06)

#include "ke06/vendor/MKE06Z4.h"
// GPIO
#define GPIO_PERIPH_TYPE GPIO_Type
//// Rotary Switch Pins
#define ROTSW_PIN1_GPIO_PORT GPIOA
constexpr uint8_t ROTSW_PIN1_GPIO_PIN = 0;
#define ROTSW_PIN2_GPIO_PORT GPIOA
constexpr uint8_t ROTSW_PIN2_GPIO_PIN = 6;
#define ROTSW_PIN4_GPIO_PORT GPIOA
constexpr uint8_t ROTSW_PIN4_GPIO_PIN = 1;
#define ROTSW_PIN8_GPIO_PORT GPIOA
constexpr uint8_t ROTSW_PIN8_GPIO_PIN = 7;
//// CAN Transceiver Pins
#define CANTRANSCVR_LPBK_GPIO_PORT GPIOA // PTC3 (see page 688 of KE06 Ref Manual)
constexpr uint8_t CANTRANSCVR_LPBK_GPIO_PIN = 19;
#define CANTRANSCVR_STDBY_GPIO_PORT GPIOA // PTC5
constexpr uint8_t CANTRANSCVR_STDBY_GPIO_PIN = 21;
//// Expander IRQ Pins
#define EXPANDERS_BUS0_IRQ_GPIO_PORT GPIOC
constexpr uint8_t EXPANDERS_BUS0_IRQ_GPIO_PIN = 5;
#define EXPANDERS_BUS1_IRQ_GPIO_PORT GPIOC
constexpr uint8_t EXPANDERS_BUS1_IRQ_GPIO_PIN = 6;
//// GPIO ports
////// NOTE: see KE06 Ref Manual page 688
////// GPIOA            , GPIOB            , GPIOC
////// 0xPTD'PTC'PTB'PTA, 0xPTH'PTG'PTF'PTE, 0x---'---'---'PTI
////// Example: ALL PTD pins are used for IO, so most significant byte of GPIOA is 0xFF
//////          NO PTC pins are used, so second most significant byte of GPIOA is 0x00
#define GPIO_PORT_1_INSTANCE GPIOA
constexpr uint32_t GPIO_PORT_1_USED = 0xFF'00'C3'3C;
#define GPIO_PORT_2_INSTANCE GPIOB
constexpr uint32_t GPIO_PORT_2_USED = 0xFF'FF'FF'FC;
#define GPIO_PORT_3_INSTANCE GPIOC
constexpr uint32_t GPIO_PORT_3_USED = 0x00'00'00'1F;

// UART
#define UART_PERIPH_TYPE UART_Type
#define UART_INSTANCE UART0

// Timer
#define TIM_PERIPH_TYPE FTM_Type
//// LEDs
#define LED_RED_TIM_INSTANCE FTM2
constexpr uint8_t LED_RED_TIM_CHANNEL = 0;
#define LED_YELLOW_TIM_INSTANCE FTM2
constexpr uint8_t LED_YELLOW_TIM_CHANNEL = 1;
#define LED_GREEN_TIM_INSTANCE FTM2
constexpr uint8_t LED_GREEN_TIM_CHANNEL = 2;

// CAN
#define CAN_CONTROLLER_PERIPH_TYPE MSCAN_Type
#define CAN_CONTROLLER_INSTANCE MSCAN

// I2C
#define I2C_PERIPH_TYPE I2C_Type
#define I2C_BUS_0_INSTANCE I2C0
#define I2C_BUS_1_INSTANCE I2C1

#elif defined(PLATFORM_TEST)
// GPIO
// TODO

#else
#error "Platform not specified"
#endif
