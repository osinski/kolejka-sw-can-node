/*
 * cpu.hpp
 * Copyright (C) 2024 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#pragma once

namespace BSP::CPU {

/**
 * @brief Configures clocks.
 *
 * @return true on success, false otherwise
 */
bool
configureClock();

/**
 * @brief Configures MCU power.
 *
 * @return true on success, false otherwise
 */
bool
configurePower();

/**
 * @brief Configures watchdog.
 *
 * @return true on success, false otherwise
 */
bool
configureWatchdog();

/**
 * @brief Configures SysTick.
 *
 * @return true on success, false otherwise
 */
bool
configureSysTick();

/**
 * @brief Configures interrupts.
 *
 * @return true on success, false otherwise
 */
bool
configureInterrupts();
}
