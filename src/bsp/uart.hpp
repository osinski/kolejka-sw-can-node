/*
 * uart.hpp
 * Copyright (C) 2024 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#pragma once

#include <array>
#include <cstddef>
#include <cstdint>

namespace BSP {
/**
 * @class UART
 * @brief Simple UART Driver class for KE06 device (no DMA, ISR based)
 *
 */
template<typename PeriphType>
class UART
{
  public:
    /**
     * @brief Constructor
     *
     * @param uartInstance Pointer to base of UART module instance.
     * @param baudrate Baudrate communication should operate with
     *                  (available baudrate depends on peripheral clock!).
     */
    UART(PeriphType* uart, uint32_t baudrate);

    /**
     * @brief UART peripheral configuration function.
     */
    void configure();

    /**
     * @brief Writes single character to the serial port
     *
     * @param c Character to write
     */
    void writeChar(char c);

    /**
     * @brief Reads single character from the serial port
     *
     * @return Character read from the serial port
     */
    char readChar();

    /**
     * @brief Sends string over the serial port
     *
     * @note Doesn't block, uses UART TDRE interrupt
     *
     * @param string \0 terminated string to be sent
     *
     * @return True if send was initiated, False if not (since other string is being sent already)
     */
    bool send(const char* string);

    /**
     * @brief Should be called when Transmit Data Register Empty interrupt is triggered. Handles
     *        write of another character from the write buffer.
     */
    void handleCharacterSent();

    /**
     * @brief Should be called when Transmit Complete interrupt is triggered. Marks write buffer
     *        as available for further transmissions.
     */
    void handleTransmissionCompleted();

    /**
     * @brief Checks if data transmission is already in progress.
     *
     * @return True if data transmission is already in progress, false if not.
     */
    [[nodiscard]] bool isTransmitting() const;

    UART() = delete;
    UART(const UART&) = delete;
    UART(UART&&) = delete;
    UART operator=(const UART&) = delete;
    UART operator=(UART&&) = delete;
    ~UART() = default;

  private:
    PeriphType* uartBase;
    uint32_t baudrate;
    volatile bool transmitting = false;
    std::array<char, 256> txBuf{};
    std::size_t posToWriteFrom = 0;
};

}
