/*
 * gpio.hpp
 * Copyright (C) 2024 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#pragma once

#include <cstdint>

namespace BSP::GPIO {
enum class LogicState
{
    LOW,
    HIGH,
};

// TODO: check if more methods can be made const
template<typename PeriphType>
class PinInput
{
  public:
    PinInput(PeriphType* port, uint8_t pin);
    PinInput(PinInput&&) = default;

    void configure();
    [[nodiscard]] LogicState read() const;

    PinInput() = delete;
    PinInput(const PinInput&) = delete;
    PinInput operator=(const PinInput&) = delete;
    PinInput operator=(PinInput&&) = delete;
    ~PinInput() = default;

  private:
    PeriphType* portBase;
    uint8_t pin;
};

template<typename PeriphType>
class PinOutput
{
  public:
    PinOutput(PeriphType* port, uint8_t pin);
    PinOutput(PinOutput&&) = default;

    void configure();
    void write(LogicState state);
    void set();
    void clear();
    void toggle();

    PinOutput() = delete;
    PinOutput(const PinOutput&) = delete;
    PinOutput operator=(const PinOutput&) = delete;
    PinOutput operator=(PinOutput&&) = delete;
    ~PinOutput() = default;

  private:
    PeriphType* portBase;
    uint8_t pin;
};

template<typename PeriphType>
class Port
{
  public:
    explicit Port(PeriphType* port);
    Port(Port&&) = default;

    void configureDirections(uint32_t dirMask) const;
    [[nodiscard]] uint32_t inputRegisterRead() const;
    void outputRegisterWrite(uint32_t newValues) const;

    Port() = delete;
    Port(const Port&) = delete;
    Port operator=(const Port&) = delete;
    Port operator=(Port&&) = delete;
    ~Port() = default;

  private:
    PeriphType* portBase;
};

void
portControlConfigure();
}
