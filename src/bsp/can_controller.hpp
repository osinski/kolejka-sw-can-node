/*
 * can_controller.hpp
 * Copyright (C) 2024 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#pragma once

#include <array>
#include <cstdint>

namespace BSP::CAN_Ctrl {

struct Frame // CAN 2.0A Frame: 11-bit ID, 0..8 bytes of data
{
    uint16_t id;
    std::array<uint8_t, 8> data;
};
enum class BusSpeed
{
    BAUD_125K = 125'000,
    BAUD_250K = 250'000,
    BAUD_500K = 500'000,
    BAUD_1M = 1'000'000,
};
enum class OperationMode
{
    LOOPBACK,
    LISTEN_ONLY,
    NORMAL
};

struct Filter
{
    uint32_t maskValue;
    uint32_t acceptanceValue;
};
enum class Error
{

};

template<typename PeriphType>
class ControllerDriver
{
  public:
    ControllerDriver(PeriphType* controller, BusSpeed busspeed);

    void configure(Filter filter, OperationMode mode) const;

    void sendFrame(const Frame& frameToSend) const;

    [[nodiscard]] Frame readReceivedFrame() const;

    ControllerDriver() = delete;
    ControllerDriver(const ControllerDriver&) = delete;
    const ControllerDriver& operator=(const ControllerDriver&) = delete;
    ControllerDriver(ControllerDriver&&) = delete;
    const ControllerDriver&& operator=(ControllerDriver&&) = delete;
    ~ControllerDriver() = default;

  private:
    PeriphType* controllerBase;
    BusSpeed speed;

    OperationMode opMode = OperationMode::LISTEN_ONLY;
};

}
