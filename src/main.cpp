/*
 * main.cpp
 * Copyright (C) 2023 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "custom_formatters.hpp"
#include "event_handlers.hpp"
#include "expander.hpp"
#include "helpers.hpp"
#include "io_mcu.hpp"
#include "io_expanders.hpp"
#include "system.hpp"

namespace OperatingMode {
/**
 * @brief Puts device into normal mode of operations.
 *
 * @param nodeAddress CAN node address this device will operate with.
 */
void
normal(Board::Configuration boardCfg);

/**
 * @brief Puts device into test mode. Shell is available in this mode.
 */
void
test();
}

[[noreturn]] int
main()
{
    System::initCorePeripherals();
    System::initBoardPeripherals();
    System::initCommPeripherals();

    const auto systemConfiguration = System::getBoardConfig();

    if (systemConfiguration == Board::Configuration::RESERVED) {
        System::getLedRed().turnOn();
        System::getShell().log<Shell::LogLevel::ERR>(
          "Nieznana/niewspierana konfiguracja urządzenia!");
        while (true) {
            __NOP();
        }
    } else if (systemConfiguration == Board::Configuration::SELF_TEST) {
        OperatingMode::test();

        // OperatingMode returned = something bad happened
        // TODO: make wdog reset the MCU?
        System::getLedRed().turnOn();
        System::getShell().log<Shell::LogLevel::ERR>("Napotkano krytyczny błąd w trybie testowym");
        while (true) {
            __NOP();
        }
    } else {
        OperatingMode::normal(systemConfiguration);

        // OperatingMode returned = something bad happened
        // TODO: make wdog reset the MCU?
        System::getLedRed().turnOn();
        System::getShell().log<Shell::LogLevel::ERR>("Napotkano krytyczny błąd w trybie normalnym");
        while (true) {
            __NOP();
        }
    }
}

namespace OperatingMode {
void
normal(Board::Configuration boardCfg)
{
    auto& shell = System::getShell();
    auto& driverCanController = System::getDriverCanCtrl();
    auto& driversI2c = System::getDriversI2C();
    auto& expandersIrqLines = System::getExpandersIrqLines();

    const auto nodeAddress = Helper::getNodeAddrFromBoardCfg(boardCfg);
    driverCanController.configure(CAN_Comm::generateFilter(nodeAddress),
                                  BSP::CAN_Ctrl::OperationMode::NORMAL);

    const IO::Expanders::Existing existingExpanders = IO::Expanders::discover(driversI2c);

    if (CAN_Comm::sendStatus() == false) {
        shell.log<Shell::LogLevel::ERR>("Wysyłanie statusu nie powiodło się");
        return;
    }

    while (true) {
        if (CAN_Comm::isFrameAvailable()) {
            const auto frame = CAN_Comm::getReceivedFrame();
            shell.log<Shell::LogLevel::DBG>(
              "Odebrano ramkę CAN: id=0x{:03X}, "
              "data=0x{:02X},0x{:02X},0x{:02X},0x{:02X},0x{:02X},0x{:02X},0x{:02X},0x{:02X}",
              frame.id.rawValue,
              frame.payload[7],
              frame.payload[6],
              frame.payload[5],
              frame.payload[4],
              frame.payload[3],
              frame.payload[2],
              frame.payload[1],
              frame.payload[0]);

            // TODO: HW filter in MSCAN should filter out frames for/from other nodes and
            // checking node address here shouldn't be necessary
            if (frame.id.nodeAddress == nodeAddress) {
                EventHandle::canFrame(frame);
            } else {
                System::getLedYellow().turnOn();
                shell.log<Shell::LogLevel::WRN>("Odebrano ramkę nieprzeznaczoną dla nas. "
                                                "Proces filtracji ramek na poziomie HW zawiódł");
            }
        }

        if (IO::MCU::haveInputsChanged(System::gpioPorts)) {
            EventHandle::ioLocalChange(nodeAddress);
        }

        for (System::I2C::BusIdx i = 0; i < System::I2C::busCount; i++) {
            if (IO::Expanders::haveInputsChanged(expandersIrqLines[i])) {
                shell.log("Wykryto zmianę stanu IO w ekspanderach na magistrali I2C{}", i);
                EventHandle::ioExpanderChange(nodeAddress);
            }
        }
    }
}

void
test()
{
    auto& shell = System::getShell();
    auto& driverCanController = System::getDriverCanCtrl();
    auto& driversI2c = System::getDriversI2C();
    auto& driverUart = System::getDriverUart();
    auto& expandersIrqLines = System::getExpandersIrqLines();
    auto& queueUartChars = System::getQueueUartChars();

    shell.print(ANSI_EscapeCodes::Decorations::bold);
    shell.println("Tryb testowy");
    shell.print(ANSI_EscapeCodes::reset);

    System::getLedRed().enableBlinking();
    System::getLedYellow().enableBlinking();
    System::getLedGreen().enableBlinking();

    const auto mscanFilter = CAN_Comm::generateFilter(CAN_Comm::NodeAddress::Address::INVALID);
    driverCanController.configure(mscanFilter, BSP::CAN_Ctrl::OperationMode::NORMAL);

    const IO::Expanders::Existing existingExpanders = IO::Expanders::discover(driversI2c);
    auto testExpander = Helper::getTestExpander(existingExpanders);

    System::I2C::Driver* testI2cBus = nullptr;
    PCAL6524::Address testExpanderAddr{};
    if (testExpander.has_value()) {
        testI2cBus = std::get<System::I2C::Driver*>(testExpander.value());
        testExpanderAddr = std::get<PCAL6524::Address>(testExpander.value());
        Expander::configureExpander(*testI2cBus, testExpanderAddr);
        Expander::configureIO(*testI2cBus, testExpanderAddr, { 0x00, 0xff, 0x00 });
    } else {
        shell.log("Nie znaleziono żadnego expandera");
    }

    char charFromUART = 0;

    // TODO: temporary test, delete later
    Expander::RegistersData expanderRegGroup;
    std::bitset<8> vals = 0x00;

    while (true) {
        if (CAN_Comm::isFrameAvailable()) {
            const auto frame = CAN_Comm::getReceivedFrame();
            shell.log(
              "Odebrano ramkę CAN: id=0x{:03X}, "
              "data=0x{:02X},0x{:02X},0x{:02X},0x{:02X},0x{:02X},0x{:02X},0x{:02X},0x{:02X}",
              frame.id.rawValue,
              frame.payload[7],
              frame.payload[6],
              frame.payload[5],
              frame.payload[4],
              frame.payload[3],
              frame.payload[2],
              frame.payload[1],
              frame.payload[0]);

            shell.println("{: >21}({})", " ", frame.id);
        }

        if (!queueUartChars.empty()) {
            charFromUART = queueUartChars.front();
            queueUartChars.pop();
            shell.receiveChar(charFromUART);
            driverUart.writeChar(charFromUART);
        }

        if (IO::MCU::haveInputsChanged(System::gpioPorts)) {
            shell.log("Wykryto zmianę stanu wejść");
        }

        if (testI2cBus != nullptr) {
            for (System::I2C::BusIdx i = 0; i < System::I2C::busCount; i++) {
                if (IO::Expanders::haveInputsChanged(expandersIrqLines[i])) {
                    shell.log("Wykryto zmianę stanu IO w ekspanderach na magistrali I2C{}", i);
                }
            }

            // TODO: temporary test, delete later
            vals.flip();
            Expander::setOutputs(*testI2cBus, testExpanderAddr, { vals, vals, vals });

            // TODO: value() may (want to) throw, change it
            expanderRegGroup =
              PCAL6524::readGroup(*testI2cBus, testExpanderAddr, PCAL6524::RegisterGroups::Input)
                .value();
            shell.log("Rejestry wejściowe expandera = 0x{:02X}, 0x{:02X}, 0x{:02X}",
                      expanderRegGroup[0],
                      expanderRegGroup[1],
                      expanderRegGroup[2]);

            expanderRegGroup =
              PCAL6524::readGroup(*testI2cBus, testExpanderAddr, PCAL6524::RegisterGroups::Output)
                .value();
            shell.log("Rejestry wyjściowe expandera = 0x{:02X}, 0x{:02X}, 0x{:02X}",
                      expanderRegGroup[0],
                      expanderRegGroup[1],
                      expanderRegGroup[2]);

            expanderRegGroup = PCAL6524::readGroup(*testI2cBus,
                                                   testExpanderAddr,
                                                   PCAL6524::RegisterGroups::Configuration)
                                 .value();

            shell.log("Rejestry kontrolne expandera = 0x{:02X}, 0x{:02X}, 0x{:02X}",
                      expanderRegGroup[0],
                      expanderRegGroup[1],
                      expanderRegGroup[2]);
            shell.println("{0:-^100}", "");

            System::waitSecs(5);
            //  TODO: end of temporary test code, delete later.
        }
    }
}
}
