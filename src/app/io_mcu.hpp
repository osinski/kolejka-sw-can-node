/*
 * io_mcu.hpp
 * Copyright (C) 2023 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#pragma once

#include "bsp/gpio.hpp"
#include "gpio_aliases.hpp"
#include "platform.hpp"

namespace IO::MCU {

using Ports =
  std::array<std::pair<BSP::GPIO::Port<GPIO_PERIPH_TYPE>, GpioMcuBits>, gpioPortsCount>;

/**
 * @brief Sets pin directions.
 *
 * @param instances Array with instances of GPIO.
 * @param newDirections Array with new directions that will be applied to pins in ports given by
 *                      instances array.
 *                      0 - pin is configured as input,
 *                      1 - pin is configured as output.
 */
void
setDirections(const Ports& instances, const GpioMcuBitsArray& newDirections);

/**
 * @brief Sets outputs.
 *
 * @param instances Array with instances of GPIO.
 * @param newOutStates Array with new states for IOs configured as outputs.
 *                     0 - output is not active,
 *                     1 - output is active.
 */
void
setOutputs(const Ports& instances, const GpioMcuBitsArray& newOutStates);

/**
 * @brief Reads inputs.
 *
 * @param instances Array with instances of GPIO.
 * @return Array with values read from IOs configured as inputs.
 *         0 - input is not active,
 *         1 - input is active.
 */
[[nodiscard]] GpioMcuBitsArray
readInputs(const Ports& instances);

/**
 * @brief Checks if inputs has changed since last check.
 *
 * @param intances Array with instances of GPIO.
 *
 * @return True if inputs have changed, false if not.
 */
[[nodiscard]] bool
haveInputsChanged(const Ports& instances);

} // namespace IO

