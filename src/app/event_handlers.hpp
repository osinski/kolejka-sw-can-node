/*
 * event_handlers.hpp
 * Copyright (C) 2024 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#pragma once

#include "can_comm.hpp"

namespace EventHandle {

void
canFrame(CAN_Comm::Frame frame);

void
ioExpanderChange(CAN_Comm::NodeAddress::Address addr);

void
ioLocalChange(CAN_Comm::NodeAddress::Address addr);
}
