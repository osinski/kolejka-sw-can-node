/*
 * board.hpp
 * Copyright (C) 2023 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#pragma once

#include <utility>

#include "bsp/gpio.hpp"
#include "bsp/timer_channel.hpp"
#include "platform.hpp"

namespace Board {

enum class Configuration
{
    SELF_TEST = 0,
    WORKING_NODE_1,
    WORKING_NODE_2,
    WORKING_NODE_3,
    WORKING_NODE_4,
    WORKING_NODE_5,
    WORKING_NODE_6,
    WORKING_NODE_7,
    WORKING_NODE_8,
    WORKING_NODE_9,
    RESERVED
};

class CANtransceiver
{
    using PinOutput = BSP::GPIO::PinOutput<GPIO_PERIPH_TYPE>;

  public:
    CANtransceiver(PinOutput&& loopbackPin, PinOutput&& standbyPin)
      : lpbkPin{ std::move(loopbackPin) }
      , stdbyPin{ std::move(standbyPin) }
    {
    }

    void configureGPIO()
    {
        lpbkPin.configure();
        stdbyPin.configure();
    }

    void enableLoopback() { lpbkPin.set(); }

    void disableLoopback() { lpbkPin.clear(); }

    void enableStandby() { stdbyPin.set(); }

    void disableStandby() { stdbyPin.clear(); }

    CANtransceiver() = delete;
    CANtransceiver(const CANtransceiver&) = delete;
    CANtransceiver(const CANtransceiver&&) = delete;
    CANtransceiver operator=(const CANtransceiver&) = delete;
    CANtransceiver operator=(const CANtransceiver&&) = delete;
    ~CANtransceiver() = default;

  private:
    PinOutput lpbkPin;
    PinOutput stdbyPin;
};

/**
 * @class RotarySwitch
 * @brief Represents BCD rotary switch.
 *
 */
class RotarySwitch
{
    using PinInput = BSP::GPIO::PinInput<GPIO_PERIPH_TYPE>;

  public:
    /**
     * @brief Constructor
     *
     * @param pin1 GPIO to which pin with BCD weight 1 is connected to
     * @param pin2 GPIO to which pin with BCD weight 2 is connected to
     * @param pin4 GPIO to which pin with BCD weight 4 is connected to
     * @param pin8 GPIO to which pin with BCD weight 8 is connscted to
     */
    RotarySwitch(PinInput&& pin1, PinInput&& pin2, PinInput&& pin4, PinInput&& pin8)
      : pin1{ std::move(pin1) }
      , pin2{ std::move(pin2) }
      , pin4{ std::move(pin4) }
      , pin8{ std::move(pin8) } {};

    /**
     * @brief Initializes GPIOs associated with the switch
     */
    void configureGPIO()
    {
        pin1.configure();
        pin2.configure();
        pin4.configure();
        pin8.configure();
    }

    /**
     * @brief Reads current configuration selected with the switch
     *
     * @return Configuration selected by the switch (value of Configuration enum class)
     */
    [[nodiscard]] Configuration read()
    {
        const unsigned weight1 = pin1.read() == BSP::GPIO::LogicState::LOW ? 1 : 0;
        const unsigned weight2 = pin2.read() == BSP::GPIO::LogicState::LOW ? 1 : 0;
        const unsigned weight4 = pin4.read() == BSP::GPIO::LogicState::LOW ? 1 : 0;
        const unsigned weight8 = pin8.read() == BSP::GPIO::LogicState::LOW ? 1 : 0;
        const unsigned raw = weight1 | (weight2 << 1UL) | (weight4 << 2UL) | (weight8 << 3UL);

        if (raw >= static_cast<unsigned>(Configuration::RESERVED)) {
            return Configuration::RESERVED;
        }
        return static_cast<Configuration>(raw);
    }

    RotarySwitch() = delete;
    RotarySwitch(const RotarySwitch&) = delete;
    RotarySwitch(const RotarySwitch&&) = delete;
    RotarySwitch operator=(const RotarySwitch&) = delete;
    RotarySwitch operator=(const RotarySwitch&&) = delete;
    ~RotarySwitch() = default;

  private:
    PinInput pin1;
    PinInput pin2;
    PinInput pin4;
    PinInput pin8;
};

class LED
{
    using TimerChannel = BSP::TimerChannel::OutputCompareDriver<TIM_PERIPH_TYPE>;

  public:
    /**
     * @brief Constructor
     *
     * @param ftmInstance Pointer to base of FTM module instance
     */
    explicit LED(TimerChannel&& channel)
      : timerChannel{ std::move(channel) }
    {
    }

    /**
     * @brief Configures FTM module for the specified LED
     */
    void configure() { timerChannel.configure(); }

    /**
     * @brief Turns LED on
     */
    void turnOn()
    {
        timerChannel.swControlEnable();
        timerChannel.swControlSetActive();
    }

    /**
     * @brief Turns LED off
     */
    void turnOff()
    {
        timerChannel.swControlSetInactive();
        timerChannel.swControlDisable();
    }

    /**
     * @brief Toggles the LED
     */
    void toggle() { timerChannel.swControlToggle(); }

    /**
     * @brief Enables blinking of the LED
     *
     * @note Turn off period = turn on period = 1s
     */
    void enableBlinking() { timerChannel.startOutputCompare(); }

    /**
     * @brief Disables blinking of the LED
     *
     * @note Turn off period = turn on period = 1s
     */
    void disableBlinking() { timerChannel.stopOutputCompare(); }

    LED() = delete;
    LED(const LED&) = delete;
    LED(const LED&&) = delete;
    LED operator=(const LED&) = delete;
    LED operator=(const LED&&) = delete;
    ~LED() = default;

  private:
    TimerChannel timerChannel;
};
}
