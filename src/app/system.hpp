/*
 * system.cpp
 * Copyright (C) 2023 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#pragma once

#include <etl/queue.h>

#include "bsp/can_controller.hpp"
#include "bsp/i2c.hpp"
#include "bsp/uart.hpp"

#include "board.hpp"
#include "io_mcu.hpp"
#include "platform.hpp"
#include "shell.hpp"

namespace System {

namespace I2C {
using BusIdx = size_t;
static constexpr size_t busCount = 2;
using Driver = BSP::I2C_Bus::I2CMasterDriver<I2C_PERIPH_TYPE>;
using DriversArray = std::array<Driver, busCount>;
}

static constexpr size_t queueSize = 16;
using QueueCanFrames =
  etl::queue<BSP::CAN_Ctrl::Frame, queueSize, etl::memory_model::MEMORY_MODEL_SMALL>;
using QueueUartChars = etl::queue<char, queueSize, etl::memory_model::MEMORY_MODEL_SMALL>;
using Shell = Shell::Shell<BSP::UART<UART_PERIPH_TYPE>>;
using UartDriver = BSP::UART<UART_PERIPH_TYPE>;
using CanControllerDriver = BSP::CAN_Ctrl::ControllerDriver<CAN_CONTROLLER_PERIPH_TYPE>;

using ExpandersIrqLinesArray = std::array<BSP::GPIO::PinInput<GPIO_PERIPH_TYPE>, I2C::busCount>;

extern const IO::MCU::Ports gpioPorts;

QueueCanFrames&
getQueueCanFrames();
QueueUartChars&
getQueueUartChars();

ExpandersIrqLinesArray&
getExpandersIrqLines();

UartDriver&
getDriverUart();
CanControllerDriver&
getDriverCanCtrl();
I2C::DriversArray&
getDriversI2C();
Shell&
getShell();

Board::LED&
getLedRed();
Board::LED&
getLedYellow();
Board::LED&
getLedGreen();

volatile uint32_t&
getSecCounter();
void
incSecCounter();

/**
 * @brief Initializes MCU core peripheras, e.g. clock.
 */
void
initCorePeripherals();

/**
 * @brief Initializes board peripherals, e.g. LEDs.
 */
void
initBoardPeripherals();

/**
 * @brief Initializes communication peripherals.
 */
void
initCommPeripherals();

/**
 * @brief Returns board configuration read by rotary switch.
 */
Board::Configuration
getBoardConfig();

void
secTick();

/**
 * @brief Blocks until specified amount of seconds has passed.
 *
 * @param secsToWait Seconds to wait (block).
 */
void
waitSecs(uint32_t secToWait);

}
