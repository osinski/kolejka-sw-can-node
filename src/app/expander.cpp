/*
 * expander.cpp
 * Copyright (C) 2023 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "expander.hpp"

bool
Expander::checkID(const System::I2C::Driver& i2cDriver, PCAL6524::Address expanderAddr)
{
    const auto id = PCAL6524::readID(i2cDriver, expanderAddr);

    if (!id.has_value()) {
        return false;
    }

    const auto idvalue = id.value();
    const auto manufacturer = std::get<0>(idvalue);
    const auto partID = std::get<1>(idvalue);
    const auto revision = std::get<2>(idvalue);

    return manufacturer == 0x0000 && partID == 0x0106 && revision == 0x00;
}

bool
Expander::configureExpander(const System::I2C::Driver& i2cDriver, PCAL6524::Address expanderAddr)
{
    // select weakest output drive-strength
    if (!PCAL6524::writeGroup<System::I2C::Driver, PCAL6524::GroupSize::Big>(
          i2cDriver,
          expanderAddr,
          PCAL6524::RegisterGroups::OutputDriveStrength,
          { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 })) {
        return false;
    }
    // enable latches for all inputs
    if (!PCAL6524::writeGroup(
          i2cDriver, expanderAddr, PCAL6524::RegisterGroups::InputLatch, { 0xFF, 0xFF, 0xFF })) {
        return false;
    }
    // unmask all interrupts
    if (!PCAL6524::writeGroup(
          i2cDriver, expanderAddr, PCAL6524::RegisterGroups::InterruptMask, { 0x00, 0x00, 0x00 })) {
        return false;
    }

    return true;
}

bool
Expander::checkPendingIRQ(const System::I2C::Driver& i2cDriver, PCAL6524::Address expanderAddr)
{
    // TODO add error handling
    const auto regs =
      PCAL6524::readGroup(i2cDriver, expanderAddr, PCAL6524::RegisterGroups::InterruptStatus)
        .value();

    return std::ranges::any_of(regs, [](const std::bitset<8> reg) { return reg.any(); });
}

bool
Expander::configureIO(const System::I2C::Driver& i2cDriver,
                      PCAL6524::Address expanderAddr,
                      Expander::RegistersData portsCfg)
{
    return PCAL6524::writeGroup(
      i2cDriver, expanderAddr, PCAL6524::RegisterGroups::Configuration, portsCfg);
}

bool
Expander::setOutputs(const System::I2C::Driver& i2cDriver,
                     PCAL6524::Address expanderAddr,
                     Expander::RegistersData newOutStates)
{
    return PCAL6524::writeGroup(
      i2cDriver, expanderAddr, PCAL6524::RegisterGroups::Output, newOutStates);
}

std::optional<Expander::RegistersData>
Expander::readInputs(const System::I2C::Driver& i2cDriver, PCAL6524::Address expanderAddr)
{
    return PCAL6524::readGroup(i2cDriver, expanderAddr, PCAL6524::RegisterGroups::Input);
}
