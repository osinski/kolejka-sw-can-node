/*
 * isr.cpp
 * Copyright (C) 2023 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "system.hpp"

extern "C"
{
    void MSCAN_1_IRQHandler(void) // MSCAN Rx Interrupts
    {
        const uint32_t flags = MSCAN->CANRFLG;
        uint32_t clearMask = 0x00;
        auto& driverCanController = System::getDriverCanCtrl();
        auto& queueCanFrames = System::getQueueCanFrames();

        // RXF = Receiver Buffer Full
        if ((flags & MSCAN_CANRFLG_RXF_MASK) != 0) {
            BSP::CAN_Ctrl::Frame recvdFrame = driverCanController.readReceivedFrame();
            queueCanFrames.push(recvdFrame);
            clearMask |= MSCAN_CANRFLG_RXF_MASK;
        }
        // OVRIF = Overrun
        if ((flags & MSCAN_CANRFLG_OVRIF_MASK) != 0) {
            clearMask |= MSCAN_CANRFLG_OVRIF_MASK;
        }
        // CAN Status Change
        if ((flags & MSCAN_CANRFLG_CSCIF_MASK) != 0) {
            clearMask |= MSCAN_CANRFLG_CSCIF_MASK;
        }
        // Wake-up
        if ((flags & MSCAN_CANRFLG_WUPIF_MASK) != 0) {
            clearMask |= MSCAN_CANRFLG_WUPIF_MASK;
        }

        MSCAN->CANRFLG = clearMask;
    }

    void UART0_IRQHandler(void)
    {
        const uint32_t statusFlags = UART0->S1;
        auto& driverUart = System::getDriverUart();
        auto& queueUartChars = System::getQueueUartChars();

        if (driverUart.isTransmitting() && ((statusFlags & UART_S1_TDRE_MASK) != 0)) {
            driverUart.handleCharacterSent();
        } else if ((statusFlags & UART_S1_RDRF_MASK) != 0) {
            const char recvdChar = driverUart.readChar();
            queueUartChars.push(recvdChar);
        } else {
            __NOP(); // rest of ISRs are not needed and are not handled
        }
    }

    void SysTick_Handler(void)
    {
        System::getShell().secTick();
        System::incSecCounter();
    }

    void HardFault_Handler(void)
    {
        System::getLedRed().turnOn();
        System::getLedGreen().turnOff();
        System::getLedYellow().enableBlinking();

        // TODO: add dump of registers stored on MSP/PSP, as described here:
        // https://interrupt.memfault.com/blog/cortex-m-hardfault-debug
        // to aid debugging
        System::getShell().log<Shell::LogLevel::ERR>("HARDFAULT!");

        while (true) {
            __NOP();
        }
    }

    void WDOG_IRQHandler(void)
    {
        while (true) {
            __NOP();
        }
    }
}
