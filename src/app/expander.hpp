/*
 * expander.hpp
 * Copyright (C) 2023 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#pragma once

#include <array>
#include <optional>

#include "pcal6524/pcal6524.hpp"
#include "system.hpp"

namespace Expander {
using RegistersData = std::array<PCAL6524::Register, 3>;

bool
checkID(const System::I2C::Driver& i2cDriver, PCAL6524::Address expanderAddr);

bool
configureExpander(const System::I2C::Driver& i2cDriver, PCAL6524::Address expanderAddr);

bool
checkPendingIRQ(const System::I2C::Driver& i2cDriver, PCAL6524::Address expanderAddr);

bool
configureIO(const System::I2C::Driver& i2cDriver,
            PCAL6524::Address expanderAddr,
            RegistersData portsCfg);

bool
setOutputs(const System::I2C::Driver& i2cDriver,
           PCAL6524::Address expanderAddr,
           RegistersData newOutStates);

std::optional<RegistersData>
readInputs(const System::I2C::Driver& i2cDriver, PCAL6524::Address expanderAddr);
}
