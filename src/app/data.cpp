/*
 * data.cpp
 * Copyright (C) 2023 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "data.hpp"

[[nodiscard]] PackedBytesArray
Data::pack(GpioMcuBitsArray vals, GpioMcuBitsArray usedPins)
{
    PackedBytesArray packed;
    size_t packedByteIdx = 0;

    for (size_t gpioIdx = 0; gpioIdx < vals.size(); ++gpioIdx) {
        for (size_t byteIdx = 0; byteIdx < vals[gpioIdx].size() / 8; ++byteIdx) {
            std::bitset<8> portBitsUsed =
              (usedPins[gpioIdx].to_ulong() & (0xFFU << (byteIdx * 8))) >> byteIdx * 8;

            if (portBitsUsed.none()) {
                continue;
            }

            std::bitset<8> portBitsPacked = 0;
            std::bitset<8> portBitsVals =
              (vals[gpioIdx].to_ulong() & (0xFFU << (byteIdx * 8))) >> byteIdx * 8;

            if (portBitsUsed.all()) {
                portBitsPacked = portBitsVals;
            } else {
                size_t packedBitIdx = 0;

                for (size_t i = 0; i < 8; ++i) {
                    if (portBitsUsed.test(i)) {
                        portBitsPacked[packedBitIdx++] = portBitsVals[i];
                    }
                }
            }

            if (packedByteIdx < packed.size()) {
                packed[packedByteIdx++] = portBitsPacked.to_ulong();
            } else {
                return packed;
            }
        }
    }

    return packed;
}

[[nodiscard]] GpioMcuBitsArray
Data::unpack(PackedBytesArray vals, GpioMcuBitsArray usedPins)
{
    GpioMcuBitsArray unpacked;
    size_t byteToUnpackIdx = 0;

    for (size_t gpioIdx = 0; gpioIdx < unpacked.size(); ++gpioIdx) {
        std::bitset<32> portBitsUnpacked = 0;
        size_t byteToWriteIdx = 0;

        for (size_t byteIdx = 0; byteIdx < unpacked[gpioIdx].size() / 8; ++byteIdx) {
            std::bitset<8> portBitsUsed =
              (usedPins[gpioIdx].to_ulong() & (0xFFU << (byteIdx * 8))) >> byteIdx * 8;

            if (portBitsUsed.none()) {
                byteToWriteIdx++;
                continue;
            }

            std::bitset<8> portBitsVals = vals[byteToUnpackIdx++];

            if (portBitsUsed.all()) {
                for (size_t i = 0; i < 8; ++i) {
                    portBitsUnpacked[byteToWriteIdx * 8 + i] = portBitsVals[i];
                }
            } else {
                size_t unpackedBitIdx = 0;

                for (size_t i = 0; i < 8; ++i) {
                    if (portBitsUsed.test(i)) {
                        portBitsUnpacked[byteToWriteIdx * 8 + i] = portBitsVals[unpackedBitIdx++];
                    }
                }
            }
            byteToWriteIdx++;
        }
        unpacked[gpioIdx] = portBitsUnpacked;
    }

    return unpacked;
}

[[nodiscard]] PackedBytesArray
Data::pack(GpioExpanderBitsArray vals)
{
    PackedBytesArray packed;

    for (size_t i = 0; i < vals.size(); ++i) {
        packed[i] = vals[i].to_ulong();
    }

    return packed;
}

[[nodiscard]] GpioExpanderBitsArray
Data::unpack(PackedBytesArray vals)
{
    GpioExpanderBitsArray unpacked;

    for (size_t i = 0; i < unpacked.size(); ++i) {
        unpacked[i] = vals[i];
    }

    return unpacked;
}
