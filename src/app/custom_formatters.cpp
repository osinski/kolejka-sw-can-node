/*
 * custom_formatters.cpp
 * Copyright (C) 2024 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "custom_formatters.hpp"

fmt::format_context::iterator
fmt::formatter<PCAL6524::Register>::format(PCAL6524::Register regdata, format_context& ctx) const
{
    return formatter<unsigned long>::format(regdata.to_ulong(), ctx);
}

fmt::format_context::iterator
fmt::formatter<PCAL6524::Address>::format(PCAL6524::Address addr, format_context& ctx) const
{
    string_view addrStr = "?";
    switch (addr) {
        case PCAL6524::Address::GND:
            addrStr = "GND";
            break;
        case PCAL6524::Address::VDD:
            addrStr = "VDD";
            break;
        case PCAL6524::Address::SDA:
            addrStr = "SDA";
            break;
        case PCAL6524::Address::SCL:
            addrStr = "SCL";
            break;
    }
    return formatter<string_view>::format(addrStr, ctx);
}

fmt::format_context::iterator
fmt::formatter<CAN_Comm::NodeAddress::Address>::format(CAN_Comm::NodeAddress::Address addr,
                                                       format_context& ctx) const
{
    string_view addrStr = "?";
    switch (addr) {
        case CAN_Comm::NodeAddress::Address::NODE_RPi:
            addrStr = "RPi";
            break;
        case CAN_Comm::NodeAddress::Address::NODE_1:
            addrStr = "1";
            break;
        case CAN_Comm::NodeAddress::Address::NODE_2:
            addrStr = "2";
            break;
        case CAN_Comm::NodeAddress::Address::NODE_3:
            addrStr = "3";
            break;
        case CAN_Comm::NodeAddress::Address::NODE_4:
            addrStr = "4";
            break;
        case CAN_Comm::NodeAddress::Address::NODE_5:
            addrStr = "5";
            break;
        case CAN_Comm::NodeAddress::Address::NODE_6:
            addrStr = "6";
            break;
        case CAN_Comm::NodeAddress::Address::NODE_7:
            addrStr = "7";
            break;
        case CAN_Comm::NodeAddress::Address::NODE_8:
            addrStr = "8";
            break;
        case CAN_Comm::NodeAddress::Address::NODE_9:
            addrStr = "9";
            break;
        case CAN_Comm::NodeAddress::Address::INVALID:
            addrStr = "?";
            break;
    }
    return formatter<string_view>::format(addrStr, ctx);
}

fmt::format_context::iterator
fmt::formatter<CAN_Comm::MessageID>::format(CAN_Comm::MessageID id, format_context& ctx) const
{
    string_view type = "?";
    string_view subtype = "?";

    switch (id.type) {
        case CAN_Comm::MessageType::Type::STATUS:
            type = "status";
            switch (std::get<CAN_Comm::MessageSubtype::Status>(id.subtype)) {
                case CAN_Comm::MessageSubtype::Status::STATUS:
                    subtype = "status";
                    break;
                case CAN_Comm::MessageSubtype::Status::INVALID:
                    break;
            }
            break;
        case CAN_Comm::MessageType::Type::IO_CONFIGURE:
            type = "konfiguracja";
            switch (std::get<CAN_Comm::MessageSubtype::Config>(id.subtype)) {
                case CAN_Comm::MessageSubtype::Config::MCU_IO_USED:
                    subtype = "MCU używane IO";
                    break;
                case CAN_Comm::MessageSubtype::Config::MCU_IO_DIRECTIONS:
                    subtype = "MCU kierunki IO";
                    break;
                case CAN_Comm::MessageSubtype::Config::EXPANDER_I2C0_0:
                    subtype = "Expander GND magistrali 0";
                    break;
                case CAN_Comm::MessageSubtype::Config::EXPANDER_I2C0_1:
                    subtype = "Expander VDD magistrali 0";
                    break;
                case CAN_Comm::MessageSubtype::Config::EXPANDER_I2C0_2:
                    subtype = "Expander SDA magistrali 0";
                    break;
                case CAN_Comm::MessageSubtype::Config::EXPANDER_I2C0_3:
                    subtype = "Expander SCL magistrali 0";
                    break;
                case CAN_Comm::MessageSubtype::Config::EXPANDER_I2C1_0:
                    subtype = "Expander GND magistrali 1";
                    break;
                case CAN_Comm::MessageSubtype::Config::EXPANDER_I2C1_1:
                    subtype = "Expander VDD magistrali 1";
                    break;
                case CAN_Comm::MessageSubtype::Config::EXPANDER_I2C1_2:
                    subtype = "Expander SDA magistrali 1";
                    break;
                case CAN_Comm::MessageSubtype::Config::EXPANDER_I2C1_3:
                    subtype = "Expander SCL magistrali 1";
                    break;
                case CAN_Comm::MessageSubtype::Config::INVALID:
                    break;
            }
            break;
        case CAN_Comm::MessageType::Type::OUTPUTS_SET:
            type = "dane (ustaw wyjścia)";
            switch (std::get<CAN_Comm::MessageSubtype::Data>(id.subtype)) {
                case CAN_Comm::MessageSubtype::Data::MCU:
                    subtype = "MCU";
                    break;
                case CAN_Comm::MessageSubtype::Data::EXPANDER_I2C0_0:
                    subtype = "Expander GND magistrali 0";
                    break;
                case CAN_Comm::MessageSubtype::Data::EXPANDER_I2C0_1:
                    subtype = "Expander VDD magistrali 0";
                    break;
                case CAN_Comm::MessageSubtype::Data::EXPANDER_I2C0_2:
                    subtype = "Expander SDA magistrali 0";
                    break;
                case CAN_Comm::MessageSubtype::Data::EXPANDER_I2C0_3:
                    subtype = "Expander SCL magistrali 0";
                    break;
                case CAN_Comm::MessageSubtype::Data::EXPANDER_I2C1_0:
                    subtype = "Expander GND magistrali 1";
                    break;
                case CAN_Comm::MessageSubtype::Data::EXPANDER_I2C1_1:
                    subtype = "Expander VDD magistrali 1";
                    break;
                case CAN_Comm::MessageSubtype::Data::EXPANDER_I2C1_2:
                    subtype = "Expander SDA magistrali 1";
                    break;
                case CAN_Comm::MessageSubtype::Data::EXPANDER_I2C1_3:
                    subtype = "Expander SCL magistrali 1";
                    break;
                case CAN_Comm::MessageSubtype::Data::INVALID:
                    break;
            }
            break;
        case CAN_Comm::MessageType::Type::INPUTS_REPORT:
            type = "dane (stan wejść)";
            switch (std::get<CAN_Comm::MessageSubtype::Data>(id.subtype)) {
                case CAN_Comm::MessageSubtype::Data::MCU:
                    subtype = "MCU";
                    break;
                case CAN_Comm::MessageSubtype::Data::EXPANDER_I2C0_0:
                    subtype = "Expander GND magistrali 0";
                    break;
                case CAN_Comm::MessageSubtype::Data::EXPANDER_I2C0_1:
                    subtype = "Expander VDD magistrali 0";
                    break;
                case CAN_Comm::MessageSubtype::Data::EXPANDER_I2C0_2:
                    subtype = "Expander SDA magistrali 0";
                    break;
                case CAN_Comm::MessageSubtype::Data::EXPANDER_I2C0_3:
                    subtype = "Expander SCL magistrali 0";
                    break;
                case CAN_Comm::MessageSubtype::Data::EXPANDER_I2C1_0:
                    subtype = "Expander GND magistrali 1";
                    break;
                case CAN_Comm::MessageSubtype::Data::EXPANDER_I2C1_1:
                    subtype = "Expander VDD magistrali 1";
                    break;
                case CAN_Comm::MessageSubtype::Data::EXPANDER_I2C1_2:
                    subtype = "Expander SDA magistrali 1";
                    break;
                case CAN_Comm::MessageSubtype::Data::EXPANDER_I2C1_3:
                    subtype = "Expander SCL magistrali 1";
                    break;
                case CAN_Comm::MessageSubtype::Data::INVALID:
                    break;
            }
            break;
        case CAN_Comm::MessageType::Type::INVALID:
            break;
    }
    return format_to(ctx.out(), "Typ: {} | Podtyp: {} | Węzeł: {}", type, subtype, id.nodeAddress);
}
