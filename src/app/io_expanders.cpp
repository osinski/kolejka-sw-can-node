/*
 * io_expanders.cpp
 * Copyright (C) 2024 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "io_expanders.hpp"
#include "expander.hpp"

namespace {
IO::Expanders::PortsData usedIo{};
}

[[nodiscard]] IO::Expanders::Existing
IO::Expanders::discover(System::I2C::DriversArray& i2cBuses)
{
    IO::Expanders::Existing existing{};
    for (System::I2C::BusIdx i = 0; i < System::I2C::busCount; i++) {
        if (Expander::checkID(i2cBuses[i], PCAL6524::Address::GND)) {
            existing[i].set(IO::Expanders::addressToIdx(PCAL6524::Address::GND));
        }

        if (Expander::checkID(i2cBuses[i], PCAL6524::Address::VDD)) {
            existing[i].set(IO::Expanders::addressToIdx(PCAL6524::Address::VDD));
        }

        if (Expander::checkID(i2cBuses[i], PCAL6524::Address::SDA)) {
            existing[i].set(IO::Expanders::addressToIdx(PCAL6524::Address::SDA));
        }

        if (Expander::checkID(i2cBuses[i], PCAL6524::Address::SCL)) {
            existing[i].set(IO::Expanders::addressToIdx(PCAL6524::Address::SCL));
        }
    }

    return existing;
}

bool
IO::Expanders::configure(IO::Expanders::Addr expanderAddr, const ConfigData& rawData)
{
    const auto busIdx = std::get<System::I2C::BusIdx>(expanderAddr);
    const auto busAddr = std::get<PCAL6524::Address>(expanderAddr);
    auto& busDriver = System::getDriversI2C()[busIdx];

    usedIo = { rawData[0], rawData[1], rawData[2] };

    return Expander::configureIO(busDriver, busAddr, { rawData[3], rawData[4], rawData[5] });
}

bool
IO::Expanders::setOutputs(IO::Expanders::Addr expanderAddr, const PortsData& rawData)
{
    const auto busIdx = std::get<System::I2C::BusIdx>(expanderAddr);
    const auto busAddr = std::get<PCAL6524::Address>(expanderAddr);
    auto& busDriver = System::getDriversI2C()[busIdx];

    return Expander::setOutputs(busDriver, busAddr, { rawData[0], rawData[1], rawData[2] });
}

[[nodiscard]] std::optional<IO::Expanders::PortsData>
IO::Expanders::readInputs(IO::Expanders::Addr expanderAddr)
{
    const auto busIdx = std::get<System::I2C::BusIdx>(expanderAddr);
    const auto busAddr = std::get<PCAL6524::Address>(expanderAddr);
    auto& busDriver = System::getDriversI2C()[busIdx];

    const auto inputs = Expander::readInputs(busDriver, busAddr);

    if (inputs.has_value()) {
        const auto inputsValues = inputs.value();
        return IO::Expanders::PortsData{ static_cast<uint8_t>(inputsValues[0].to_ulong()),
                                         static_cast<uint8_t>(inputsValues[1].to_ulong()),
                                         static_cast<uint8_t>(inputsValues[2].to_ulong()) };
    }

    return std::nullopt;
}
