/*
 * gpio_aliases.hpp
 * Copyright (C) 2023 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#pragma once

#include <array>
#include <bitset>
#include <cstdint>

static constexpr size_t gpioPortsCount = 3;

using GpioMcuBits = std::bitset<32>;
using GpioExpanderBits = std::bitset<8>;
using GpioMcuBitsArray = std::array<GpioMcuBits, gpioPortsCount>;
using GpioExpanderBitsArray = std::array<GpioExpanderBits, gpioPortsCount>;
using PackedBytesArray = std::array<uint8_t, 8>;
