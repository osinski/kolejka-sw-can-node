/*
 * io_expanders.hpp
 * Copyright (C) 2024 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#pragma once

#include "system.hpp"
#include "pcal6524/pcal6524.hpp"

namespace IO::Expanders {
constexpr unsigned maxExpandersOnBus = 4;

using ExistingOnBus = std::bitset<maxExpandersOnBus>;
using Existing = std::array<IO::Expanders::ExistingOnBus, System::I2C::busCount>;
using Addr = std::pair<System::I2C::BusIdx, PCAL6524::Address>;
using ConfigData = std::array<uint8_t, 6>;
using PortsData = std::array<uint8_t, 3>;

[[nodiscard]] Existing
discover(System::I2C::DriversArray& i2cBuses);

bool
configure(Addr expanderAddr, const ConfigData& rawData);

bool
setOutputs(Addr expanderAddr, const PortsData& rawData);

[[nodiscard]] std::optional<PortsData>
readInputs(Addr expanderAddr);

[[nodiscard]] static inline bool
haveInputsChanged(BSP::GPIO::PinInput<GPIO_Type>& irqPin)
{
    return irqPin.read() == BSP::GPIO::LogicState::LOW;
}

[[nodiscard]] static inline size_t
addressToIdx(PCAL6524::Address addr)
{
    switch (addr) {
        case PCAL6524::Address::GND:
            return 0;
        case PCAL6524::Address::VDD:
            return 1;
        case PCAL6524::Address::SDA:
            return 2;
        case PCAL6524::Address::SCL:
            return 3;
    }
    // default case
    return 0;
}

[[nodiscard]] static inline std::optional<PCAL6524::Address>
idxToAddr(size_t idx)
{
    switch (idx) {
        case 0:
            return PCAL6524::Address::GND;
        case 1:
            return PCAL6524::Address::VDD;
        case 2:
            return PCAL6524::Address::SDA;
        case 3:
            return PCAL6524::Address::SCL;
        default:
            return std::nullopt;
    }
}
}
