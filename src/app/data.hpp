/*
 * data.hpp
 * Copyright (C) 2023 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef DATA_H
#define DATA_H

#include "gpio_aliases.hpp"

namespace Data {

/**
 * @brief [TODO:description]
 *
 * @param vals [TODO:parameter]
 * @param usedPins [TODO:parameter]
 * @return [TODO:return]
 */
[[nodiscard]] PackedBytesArray
pack(GpioMcuBitsArray vals, GpioMcuBitsArray usedPins);

/**
 * @brief [TODO:description]
 *
 * @param vals [TODO:parameter]
 * @param usedPins [TODO:parameter]
 * @return [TODO:return]
 */
[[nodiscard]] GpioMcuBitsArray
unpack(PackedBytesArray vals, GpioMcuBitsArray usedPins);

/**
 * @brief [TODO:description]
 *
 * @param vals [TODO:parameter]
 * @param usedPins [TODO:parameter]
 * @return [TODO:return]
 */
[[nodiscard]] PackedBytesArray
pack(GpioExpanderBitsArray vals);

/**
 * @brief [TODO:description]
 *
 * @param vals [TODO:parameter]
 * @param usedPins [TODO:parameter]
 * @return [TODO:return]
 */
[[nodiscard]] GpioExpanderBitsArray
unpack(PackedBytesArray vals);

} // namespace Data

#endif // DATA_H
