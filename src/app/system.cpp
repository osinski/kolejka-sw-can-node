/*
 * system.cpp
 * Copyright (C) 2023 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "system.hpp"

#include "bsp/cpu.hpp"

const IO::MCU::Ports System::gpioPorts{
    std::pair{ BSP::GPIO::Port{ GPIO_PORT_1_INSTANCE }, GPIO_PORT_1_USED },
    std::pair{ BSP::GPIO::Port{ GPIO_PORT_2_INSTANCE }, GPIO_PORT_2_USED },
    std::pair{ BSP::GPIO::Port{ GPIO_PORT_3_INSTANCE }, GPIO_PORT_3_USED }
};

System::QueueCanFrames&
System::getQueueCanFrames()
{
    static System::QueueCanFrames queue{};
    return queue;
}

System::QueueUartChars&
System::getQueueUartChars()
{
    static System::QueueUartChars queue{};
    return queue;
}

System::ExpandersIrqLinesArray&
System::getExpandersIrqLines()
{
    static System::ExpandersIrqLinesArray irqLines{
        BSP::GPIO::PinInput{ EXPANDERS_BUS0_IRQ_GPIO_PORT, EXPANDERS_BUS0_IRQ_GPIO_PIN },
        BSP::GPIO::PinInput{ EXPANDERS_BUS1_IRQ_GPIO_PORT, EXPANDERS_BUS1_IRQ_GPIO_PIN },
    };
    return irqLines;
}

// TODO: uart shouldn't be usable on its own - hide it completely behind shell
System::UartDriver&
System::getDriverUart()
{
    static constexpr uint32_t uartBaudrate = 38'400;
    static System::UartDriver driverUART{ UART_INSTANCE, uartBaudrate };
    return driverUART;
}

System::CanControllerDriver&
System::getDriverCanCtrl()
{

    static System::CanControllerDriver driverCanController{ CAN_CONTROLLER_INSTANCE,
                                                            BSP::CAN_Ctrl::BusSpeed::BAUD_125K };
    return driverCanController;
}

System::I2C::DriversArray&
System::getDriversI2C()
{
    static constexpr uint32_t i2cBusFreq = 10'000;
    static System::I2C::DriversArray driversI2C{
        BSP::I2C_Bus::I2CMasterDriver{ I2C_BUS_0_INSTANCE, i2cBusFreq },
        BSP::I2C_Bus::I2CMasterDriver{ I2C_BUS_1_INSTANCE, i2cBusFreq },
    };
    return driversI2C;
};

System::Shell&
System::getShell()
{
    static System::Shell shell{ System::getDriverUart() };
    return shell;
}

Board::LED&
System::getLedRed()
{
    static Board::LED redLed{ BSP::TimerChannel::OutputCompareDriver{
      LED_RED_TIM_INSTANCE, LED_RED_TIM_CHANNEL, BSP::TimerChannel::Polarity::LOW } };

    return redLed;
}

Board::LED&
System::getLedYellow()
{
    static Board::LED yellowLED{ BSP::TimerChannel::OutputCompareDriver{
      LED_YELLOW_TIM_INSTANCE, LED_YELLOW_TIM_CHANNEL, BSP::TimerChannel::Polarity::LOW } };

    return yellowLED;
}

Board::LED&
System::getLedGreen()
{
    static Board::LED greenLED{ BSP::TimerChannel::OutputCompareDriver{
      LED_GREEN_TIM_INSTANCE, LED_GREEN_TIM_CHANNEL, BSP::TimerChannel::Polarity::LOW } };

    return greenLED;
}

volatile uint32_t&
System::getSecCounter()
{
    static volatile uint32_t secCounter;
    return secCounter;
}

void
System::incSecCounter()
{
    System::getSecCounter()++;
}

void
System::initCorePeripherals()
{
    BSP::CPU::configureClock();
    BSP::CPU::configureSysTick();
    BSP::CPU::configurePower();
    BSP::CPU::configureInterrupts();

    if constexpr (USE_WATCHDOG) {
        BSP::CPU::configureWatchdog();
    }
}

void
System::initBoardPeripherals()
{
    getLedRed().configure();
    getLedYellow().configure();
    getLedGreen().configure();

    Board::CANtransceiver transceiverCAN{
        BSP::GPIO::PinOutput{ CANTRANSCVR_LPBK_GPIO_PORT, CANTRANSCVR_LPBK_GPIO_PIN },
        BSP::GPIO::PinOutput{ CANTRANSCVR_STDBY_GPIO_PORT, CANTRANSCVR_STDBY_GPIO_PIN },
    };
    transceiverCAN.configureGPIO();
    transceiverCAN.disableStandby();
    transceiverCAN.disableLoopback();
}

void
System::initCommPeripherals()
{
    System::getDriverUart().configure();

    auto& i2cDrivers = System::getDriversI2C();
    auto& expandersIrqLines = System::getExpandersIrqLines();
    for (uint8_t i = 0; i < System::I2C::busCount; i++) {
        i2cDrivers[i].configure();
        expandersIrqLines[i].configure();
    }

    System::getShell().initialize("Kolejka - Węzeł CAN\r\nMarcin Osiński 2020-2024");
}

Board::Configuration
System::getBoardConfig()
{
    Board::RotarySwitch rotarySwitch{
        BSP::GPIO::PinInput{ ROTSW_PIN1_GPIO_PORT, ROTSW_PIN1_GPIO_PIN },
        BSP::GPIO::PinInput{ ROTSW_PIN2_GPIO_PORT, ROTSW_PIN2_GPIO_PIN },
        BSP::GPIO::PinInput{ ROTSW_PIN4_GPIO_PORT, ROTSW_PIN4_GPIO_PIN },
        BSP::GPIO::PinInput{ ROTSW_PIN8_GPIO_PORT, ROTSW_PIN8_GPIO_PIN },
    };
    rotarySwitch.configureGPIO();

    return rotarySwitch.read();
}

void
System::waitSecs(uint32_t secToWait)
{
    auto& secCounter = System::getSecCounter();
    uint32_t sec = secCounter;

    for (uint32_t i = 0; i < secToWait; ++i) {
        while (sec == secCounter) {
            __NOP();
        }
        sec = secCounter;
    }
}
