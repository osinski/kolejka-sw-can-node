/*
 * custom_formatters.hpp
 * Copyright (C) 2024 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#pragma once

#include "fmt/format.h"

#include "pcal6524/pcal6524.hpp"
#include "can_comm.hpp"

template<>
struct fmt::formatter<PCAL6524::Register> : formatter<unsigned long>
{
    format_context::iterator format(PCAL6524::Register regdata, format_context& ctx) const;
};

template<>
struct fmt::formatter<PCAL6524::Address> : formatter<string_view>
{
    format_context::iterator format(PCAL6524::Address addr, format_context& ctx) const;
};

template<>
struct fmt::formatter<CAN_Comm::NodeAddress::Address> : formatter<string_view>
{
    format_context::iterator format(CAN_Comm::NodeAddress::Address addr, format_context& ctx) const;
};

template<>
struct fmt::formatter<CAN_Comm::MessageID> : formatter<string_view>
{
    format_context::iterator format(CAN_Comm::MessageID id, format_context& ctx) const;
};
