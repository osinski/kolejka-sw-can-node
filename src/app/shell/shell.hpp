/*
 * shell.hpp
 * Copyright (C) 2023 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef SHELL_H
#define SHELL_H

#include <cstdarg>
#include <string_view>

#include "etl/delegate.h"
#include "etl/string.h"
#include "fmt/format.h"
#include "fmt/compile.h"

#include "ansi-escape-codes.hpp"
#include "git_version.hpp"

namespace Shell {

enum LogLevel
{
    ERR,
    WRN,
    INF,
    DBG,
};

#if !defined(GLOBAL_LOG_LVL)
#define GLOBAL_LOG_LVL LogLevel::INF
#endif

struct LogTimeStampData
{
    uint32_t days;
    uint16_t seconds;
    uint8_t hours;
};

struct Command
{
    std::string_view name;
    std::string_view help;
    etl::delegate<void(void*)> callbackFunc;
};

template<class DriverUART, size_t BufSize = 256>
class Shell
{
  public:
    explicit Shell(DriverUART& driver)
      : uart{ driver }
    {
    }

    Shell() = delete;
    Shell(const Shell&) = delete;
    Shell(const Shell&&) = delete;
    Shell operator=(const Shell&) = delete;
    Shell operator=(const Shell&&) = delete;
    ~Shell() = default;

    void initialize(std::string_view helloStr);
    bool registerCommand(const Command& cmd);

    void receiveChar(char c);

    template<typename... Args>
    void print(fmt::format_string<Args...> fmt, Args&&... args);

    template<typename... Args>
    void println(fmt::format_string<Args...> fmt, Args&&... args);

    template<LogLevel loglvl = LogLevel::INF, typename... Args>
    void log(fmt::format_string<Args...> fmt, Args&&... args);

    void secTick();

    [[nodiscard]] size_t getBufferSizes() const { return BufSize; }

  private:
    etl::string<BufSize> recvBuf{};
    etl::string<BufSize> sendBuf{};
    DriverUART& uart;
    LogTimeStampData timestampData{};

    template<typename... Args>
    bool writeToBuffer(fmt::format_string<Args...> fmt, Args&&... args);
    void flushBuffer();
};

template<class DriverUART, size_t BufSize>
void
Shell<DriverUART, BufSize>::initialize(std::string_view helloStr)
{
    writeToBuffer("{}{}{}{}{}\r\nv. {}/{}\r\n\n\n{}",
                  ANSI_EscapeCodes::reset,
                  ANSI_EscapeCodes::Controls::clearEntireScreen,
                  ANSI_EscapeCodes::Controls::cursorScreenOrigins,
                  ANSI_EscapeCodes::Decorations::bold,
                  helloStr,
                  GIT_CommitHash,
                  GIT_CommitDate,
                  ANSI_EscapeCodes::reset);

    flushBuffer();
}

template<class DriverUART, size_t BufSize>
template<typename... Args>
void
Shell<DriverUART, BufSize>::print(fmt::format_string<Args...> fmt, Args&&... args)
{
    writeToBuffer(fmt, std::forward<Args>(args)...);
    flushBuffer();
}

template<class DriverUART, size_t BufSize>
template<typename... Args>
void
Shell<DriverUART, BufSize>::println(fmt::format_string<Args...> fmt, Args&&... args)
{
    writeToBuffer(fmt, std::forward<Args>(args)...);
    writeToBuffer("\r\n");
    flushBuffer();
}

template<class DriverUART, size_t BufSize>
void
Shell<DriverUART, BufSize>::secTick()
{
    if (++this->timestampData.seconds == 60 * 60) {
        if (++this->timestampData.hours == 24) {
            ++this->timestampData.days;
            this->timestampData.hours = 0;
        }
        this->timestampData.seconds = 0;
    }
}

template<class DriverUART, size_t BufSize>
template<LogLevel loglvl, typename... Args>
void
Shell<DriverUART, BufSize>::log(fmt::format_string<Args...> fmt, Args&&... args)
{
    if constexpr (loglvl <= GLOBAL_LOG_LVL) {
        std::string_view loglvlstr = "";
        std::string_view formatstr = "";
        if constexpr (loglvl == LogLevel::ERR) {
            formatstr = ANSI_EscapeCodes::Colors::Foreground::red;
            loglvlstr = "err";
        } else if constexpr (loglvl == LogLevel::WRN) {
            formatstr = ANSI_EscapeCodes::Colors::Foreground::yellow;
            loglvlstr = "wrn";
        } else if constexpr (loglvl == LogLevel::INF) {
            formatstr = ANSI_EscapeCodes::Colors::Foreground::white;
            loglvlstr = "inf";
        } else if constexpr (loglvl == LogLevel::DBG) {
            formatstr = ANSI_EscapeCodes::Colors::Foreground::blue;
            loglvlstr = "dbg";
        }

        writeToBuffer("{}[{:04}:{:02}:{:04}] <{}> ",
                      formatstr,
                      timestampData.days,
                      timestampData.hours,
                      timestampData.seconds,
                      loglvlstr);
        writeToBuffer(fmt, std::forward<Args>(args)...);
        writeToBuffer("{}\r\n", ANSI_EscapeCodes::reset);
        flushBuffer();
    }
}

template<class DriverUART, size_t BufSize>
template<typename... Args>
bool
Shell<DriverUART, BufSize>::writeToBuffer(fmt::format_string<Args...> fmt, Args&&... args)
{
    fmt::format_to(etl::back_inserter(sendBuf), fmt, std::forward<Args>(args)...);
    return true;
}

template<class DriverUART, size_t BufSize>
void
Shell<DriverUART, BufSize>::flushBuffer()
{
    while (uart.isTransmitting()) {
    }

    uart.send(sendBuf.c_str());
    sendBuf.clear();
}

template<class DriverUART, size_t BufSize>
void
Shell<DriverUART, BufSize>::receiveChar(char c)
{
    (void)c;
}

template<class DriverUART, size_t BufSize>
bool
Shell<DriverUART, BufSize>::registerCommand(const Command& cmd)
{
    (void)cmd;

    return false;
}

}

#endif /* SHELL_H */
