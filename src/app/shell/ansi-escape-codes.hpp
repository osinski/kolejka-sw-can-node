/*
 * ansi-escape-codes.hpp
 * Copyright (C) 2023 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef ANSI_ESCAPE_CODES_H
#define ANSI_ESCAPE_CODES_H

#include <string_view>

// #include ""

namespace ANSI_EscapeCodes {
namespace Colors {
namespace Foreground {
static constexpr std::string_view black{ "\033[30m" };
static constexpr std::string_view gray{ "\033[30;1m" };
static constexpr std::string_view red{ "\033[31m" };
static constexpr std::string_view brightRed{ "\033[31;1m" };
static constexpr std::string_view green{ "\033[32m" };
static constexpr std::string_view brightGreen{ "\033[32;1m" };
static constexpr std::string_view yellow{ "\033[33m" };
static constexpr std::string_view brightYellow{ "\033[33;1m" };
static constexpr std::string_view blue{ "\033[34m" };
static constexpr std::string_view brightBlue{ "\033[34;1m" };
static constexpr std::string_view magenta{ "\033[35m" };
static constexpr std::string_view brightMagenta{ "\033[35;1m" };
static constexpr std::string_view cyan{ "\033[36m" };
static constexpr std::string_view brightCyan{ "\033[36;1m" };
static constexpr std::string_view white{ "\033[37m" };
static constexpr std::string_view brightWhite{ "\033[37;1m" };
}

namespace Background {
static constexpr std::string_view black{ "\033[40m" };
static constexpr std::string_view gray{ "\033[40;1m" };
static constexpr std::string_view red{ "\033[41m" };
static constexpr std::string_view brightRed{ "\033[41;1m" };
static constexpr std::string_view green{ "\033[42m" };
static constexpr std::string_view brightGreen{ "\033[42;1m" };
static constexpr std::string_view yellow{ "\033[43m" };
static constexpr std::string_view brightYellow{ "\033[43;1m" };
static constexpr std::string_view blue{ "\033[44m" };
static constexpr std::string_view brightBlue{ "\033[44;1m" };
static constexpr std::string_view magenta{ "\033[45m" };
static constexpr std::string_view brightMagenta{ "\033[45;1m" };
static constexpr std::string_view cyan{ "\033[46m" };
static constexpr std::string_view brightCyan{ "\033[46;1m" };
static constexpr std::string_view white{ "\033[47m" };
static constexpr std::string_view brightWhite{ "\033[47;1m" };
}
}

namespace Decorations {
static constexpr std::string_view bold{ "\033[1m" };
static constexpr std::string_view underline{ "\033[4m" };
static constexpr std::string_view inverted{ "\033[7m" };
static constexpr std::string_view italic{ "\033[3m" };
}

namespace Controls {
static constexpr std::string_view clearEntireScreen{ "\033[2J" };
static constexpr std::string_view clearScreenBeforeCursor{ "\033[1J" };
static constexpr std::string_view clearScreenAfterCursor{ "\033[0J" };
static constexpr std::string_view clearEntireLine{ "\033[2K" };
static constexpr std::string_view clearLineBeforeCursor{ "\033[1K" };
static constexpr std::string_view clearLineAfterCursor{ "\033[0K" };
static constexpr std::string_view cursorScreenOrigins{ "\033[H" };
}

namespace CursorNavigation {
static constexpr std::string_view up{ "\033[A" };
static constexpr std::string_view down{ "\033[B" };
static constexpr std::string_view right{ "\033[C" };
static constexpr std::string_view left{ "\033[D" };
}

static constexpr std::string_view reset{ "\033[0m" };
}

#endif /* ANSI_ESCAPE_CODES_H */
