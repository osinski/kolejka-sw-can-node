/*
 * git_version.hpp
 * Copyright (C) 2023 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef GIT_VERSION_H
#define GIT_VERSION_H

static const char* GIT_CommitHash = "f3fbc96";
static const char* GIT_CommitDate = "2024-09-28";


#endif /* GIT_VERSION_H */

