/*
 * io_mcu.cpp
 * Copyright (C) 2023 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "io_mcu.hpp"

void
IO::MCU::setDirections(const Ports& instances, const GpioMcuBitsArray& newDirections)
{
    for (size_t i = 0; i < instances.size(); ++i) {
        const auto& port = std::get<0>(instances[i]);
        const auto& usedPins = std::get<1>(instances[i]);
        port.configureDirections(newDirections[i].to_ulong() & usedPins.to_ulong());
    }
}

void
IO::MCU::setOutputs(const Ports& instances, const GpioMcuBitsArray& newOutStates)
{
    for (size_t i = 0; i < instances.size(); ++i) {
        const auto& port = std::get<0>(instances[i]);
        const auto& usedPins = std::get<1>(instances[i]);
        port.outputRegisterWrite(newOutStates[i].to_ulong() & usedPins.to_ulong());
    }
}

[[nodiscard]] GpioMcuBitsArray
IO::MCU::readInputs(const Ports& instances)
{
    GpioMcuBitsArray result = {};

    for (size_t i = 0; i < instances.size(); ++i) {
        const auto& port = std::get<0>(instances[i]);
        const auto& usedPins = std::get<1>(instances[i]);
        result[i] = port.inputRegisterRead() & usedPins.to_ulong();
    }

    return result;
}

[[nodiscard]] bool
IO::MCU::haveInputsChanged(const Ports& instances)
{
    static GpioMcuBitsArray oldReads = readInputs(instances);
    GpioMcuBitsArray newReads = readInputs(instances);

    if (oldReads == newReads) {
        return false;
    }
    oldReads = newReads;
    return true;
}

