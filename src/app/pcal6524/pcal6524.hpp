/*
 * pcal6524.hpp
 * Copyright (C) 2023 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef PCAL6524_H
#define PCAL6524_H

#include <array>
#include <bitset>
#include <cstdint>
#include <optional>
#include <tuple>

namespace PCAL6524 {

using Register = std::bitset<8>;

constexpr unsigned maximumExpandersOnBus = 4;

enum class RegisterGroups
{
    Input = 0x00,
    Output = 0x04,
    PolarityInversion = 0x08,
    Configuration = 0x0C,
    OutputDriveStrength = 0x40,
    InputLatch = 0x48,
    PullEnable = 0x4C,
    PullSelection = 0x40,
    InterruptMask = 0x54,
    InterruptStatus = 0x58,
    OutputConfiguration = 0x5C,
    InterruptEdge = 0x60,
    InterruptClear = 0x68,
    InputStatus = 0x6C,
    IndividualPinOutputConfig = 0x70,
    SwitchDebounceEnable = 0x74,
    SwitchDebounceCount = 0x76,
};

enum GroupSize
{
    Small = 3,
    Big = 6,
};
enum class Port
{
    P0 = 0x00U,
    P0A = 0x00U,
    P0B = P0A | 0x01U,
    P1 = 0x01U,
    P1A = 0x02U,
    P1B = P1A | 0x01U,
    P2 = 0x02U,
    P2A = 0x04U,
    P2B = P2A | 0x01U,
};

enum class Address
{
    SCL = 0x40U >> 1U,
    SDA = 0x42U >> 1U,
    GND = 0x44U >> 1U,
    VDD = 0x46U >> 1U,
};

enum class PinConfiguration
{
    Output = 0,
    Input = 1
};

enum class DriveStrenghts
{
    Quarter = 0,
    Half = 1,
    ThreeQuarters = 2,
    Full = 3,
};

enum class InputLatch
{
    Disabled = 0,
    Enabled = 1
};

enum class PullResistors
{
    Disconnected = 0,
    Connected = 1,
};

enum class PullType
{
    Down = 0,
    Up = 1,
};

enum class PinIrqSettings
{
    Unmasked = 0,
    Masked = 1,
};

enum class PortConfigs
{
    PushPull = 0,
    OpenDrain = 1,
};

enum class PinIrqEdgeSettings
{
    Level = 0,
    RisingEdge = 1,
    FallingEdge = 2,
    BothEdges = 3,
};

enum class PinSwitchDebounce
{
    Disabled = 0,
    Enabled = 1,
};

/**
 * @brief Writes single register of PCAL6524.
 *
 * @tparam I2C_Driver I2C Driver to be used to communicate with PCAL6524
 * @param i2cDriver I2C Driver to be used to communicate with PCAL6524
 * @param addr Address of the expander
 * @param group Group the register belongs to
 * @param port Which port the register is associated with
 * @param data Data to be written
 * @return Whether write process was successful
 * @retval false Bus error occured while writing register group
 * @retval true Write process was succesful
 *
 * @note For registers that don't have different instances for each port (e.g.
 *       `Output port configuration register`) set port as P0
 */
template<class I2C_Driver>
bool
writeRegSingle(const I2C_Driver& i2cDriver,
               Address addr,
               RegisterGroups group,
               Port port,
               Register data)
{
    const std::array dataToWrite = { static_cast<uint8_t>(static_cast<int>(group) |
                                                          static_cast<int>(port)),
                                     static_cast<uint8_t>(data.to_ulong()) };

    auto err = i2cDriver.masterStartWrite(static_cast<uint8_t>(addr));
    if (err) {
        return false;
    }
    err = i2cDriver.masterWriteAndStop(dataToWrite);
    if (err) {
        return false;
    }
    return true;
}

/**
 * @brief Writes all registers in the group.
 *
 * @tparam I2C_Driver I2C Driver to be used to communicate with PCAL6524
 * @tparam size Size of the register group (use GroupSize enum)
 * @param i2cDriver I2C Driver to be used to communicate with PCAL6524
 * @param addr Address of the expander
 * @param group Group whose registers will be written to
 * @param data Data to be written
 * @return Whether write process was successful
 * @retval false Bus error occured while writing register group
 * @retval true Write process was succesful
 *
 * @note Output port configuration register doesn't belong to any group.
 *       Use `writeRegSingle` to write to that register.
 *       Refer to PCAL6524 datasheet for more info.
 */
template<class I2C_Driver, size_t size = GroupSize::Small>
bool
writeGroup(const I2C_Driver& i2cDriver,
           Address addr,
           RegisterGroups group,
           const std::array<Register, size>& data)
{
    auto err = i2cDriver.masterStartWrite(static_cast<uint8_t>(addr));
    if (err) {
        return false;
    }
    err = i2cDriver.masterWriteSubaddress(static_cast<uint8_t>(group));
    if (err) {
        return false;
    }

    std::array<uint8_t, size> dataToWrite{};
    for (size_t i = 0; i < size; i++) {
        dataToWrite[i] = data[i].to_ulong();
    }

    err = i2cDriver.masterWriteAndStop(dataToWrite);
    if (err) {
        return false;
    }
    return true;
}

/**
 * @brief Reads single register of PCAL6524.
 *
 * @tparam I2C_Driver I2C Driver to be used to communicate with PCAL6524
 * @param i2cDriver I2C Driver to be used to communicate with PCAL6524
 * @param addr Address of the expander
 * @param group Group the register belongs to
 * @param port Which port the register is associated with
 * @return Value read from the register, if read was successful
 */
template<class I2C_Driver>
[[nodiscard]] std::optional<Register>
readRegSingle(const I2C_Driver& i2cDriver, Address addr, RegisterGroups group, Port port)
{
    auto err = i2cDriver.masterStartWrite(static_cast<uint8_t>(addr));
    if (err) {
        return {};
    }
    err = i2cDriver.masterWriteSubaddress(static_cast<uint8_t>(group) | static_cast<uint8_t>(port));
    if (err) {
        return {};
    }
    err = i2cDriver.masterRepeatStartRead(static_cast<uint8_t>(addr));
    if (err) {
        return {};
    }
    std::array<uint8_t, 1> readbuf{};
    err = i2cDriver.masterReadAndStop(readbuf);
    if (err) {
        return {};
    }
    return Register{ readbuf[0] };
}

/**
 * @brief Read all registers in the group
 *
 * @tparam I2C_Driver I2C Driver to be used to communicate with PCAL6524
 * @tparam size Size of the register group (use GroupSize enum)
 * @param i2cDriver I2C Driver to be used to communicate with PCAL6524
 * @param addr Address of the expander
 * @param group Group whose registers will be written to
 * @return Values read from registers, if read was successful
 */
template<class I2C_Driver, size_t size = GroupSize::Small>
[[nodiscard]] std::optional<std::array<Register, size>>
readGroup(const I2C_Driver& i2cDriver, Address addr, RegisterGroups group)
{
    auto err = i2cDriver.masterStartWrite(static_cast<uint8_t>(addr));
    if (err) {
        return {};
    }
    err = i2cDriver.masterWriteSubaddress(static_cast<uint8_t>(group));
    if (err) {
        return {};
    }
    err = i2cDriver.masterRepeatStartRead(static_cast<uint8_t>(addr));
    if (err) {
        return {};
    }
    std::array<uint8_t, size> readbuf{};
    err = i2cDriver.masterReadAndStop(readbuf);
    if (err) {
        return {};
    }
    std::array<Register, size> groupValues{};
    for (size_t i = 0; i < size; i++) {
        groupValues[i] = readbuf[i];
    }
    return groupValues;
}

using ID_Fields = std::tuple<uint16_t, uint16_t, uint8_t>;

/**
 * @brief Reads Device ID of the PCAL6524 Expander
 *
 * @tparam I2C_Driver I2C Driver to be used to communicate with PCAL6524
 * @param i2cDriver I2C Driver to be used to communicate with PCAL6524
 * @param addr Address of the expander
 * @return Tuple containing Manufacturer Name, Part ID and Die Revision read
 *         from the device, if read was successful
 *
 * @note According to rev. 2 of the Datasheet (15 may 2019) correct values are:
 *       Manufacturer Name = 0x0000
 *       Part ID = 0x0106
 *       Die Revision = 0x00
 */
template<class I2C_Driver>
[[nodiscard]] std::optional<ID_Fields>
readID(const I2C_Driver& i2cDriver, Address addr)
{
    static constexpr size_t readSize = 3;

    auto err = i2cDriver.masterStartWrite(0x7C);
    if (err) {
        return {};
    }
    err = i2cDriver.masterWriteSubaddress(static_cast<uint8_t>(addr) << 1);
    if (err) {
        return {};
    }
    err = i2cDriver.masterRepeatStartRead(0x7C);
    if (err) {
        return {};
    }
    std::array<uint8_t, readSize> bytes{};
    err = i2cDriver.masterReadAndStop(bytes);
    if (err) {
        return {};
    }

    return std::tuple<uint16_t, uint16_t, uint8_t>{ (bytes[0] << 4) | ((bytes[1] & 0xF0) >> 4),
                                                    ((bytes[1] & 0x0F) << 5) |
                                                      ((bytes[2] & 0xF8) >> 3),
                                                    (bytes[2] & 0x07) };
}

/**
 * @brief Uses General Call address (0x00) to reset all PCAL6524 devices present on the bus.
 *
 * @tparam I2C_Driver I2C Driver to be used to communicate with PCAL6524
 * @param i2cDriver I2C Driver to be used to communicate with PCAL6524
 * @return Whether reset procedure was successful
 * @retval false Bus error occured while performing reset procedure
 * @retval true Reset procedure was successful
 */
template<class I2C_Driver>
bool
reset(const I2C_Driver& i2cDriver)
{
    auto err = i2cDriver.masterStartWrite(0x00);
    if (err) {
        return false;
    }
    err = i2cDriver.masterWriteAndStop({ 0x06 });
    if (err) {
        return false;
    }
    return true;
}

}

#endif /* PCAL6524_H */
