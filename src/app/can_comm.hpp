/*
 * can_comm.hpp
 * Copyright (C) 2023 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#pragma once

#include <array>
#include <variant>

#include "bsp/can_controller.hpp"
#include "system.hpp"

namespace CAN_Comm {

namespace NodeAddress {
enum class Address
{
    NODE_RPi = 0,
    NODE_1,
    NODE_2,
    NODE_3,
    NODE_4,
    NODE_5,
    NODE_6,
    NODE_7,
    NODE_8,
    NODE_9,

    INVALID,
};
static constexpr uint32_t POS_SHIFT = 0;
static constexpr uint32_t MASK = 0b000'0000'1111;
}

namespace MessageType {
enum class Type
{
    STATUS = 0,
    IO_CONFIGURE,
    OUTPUTS_SET,
    INPUTS_REPORT,

    INVALID,

};
static constexpr uint32_t POS_SHIFT = 8;
static constexpr uint32_t MASK = 0b111'0000'0000;
}

namespace MessageSubtype {
enum class Status
{
    STATUS = 0,

    INVALID,
};

enum class Config
{
    MCU_IO_USED = 0,
    MCU_IO_DIRECTIONS,
    EXPANDER_I2C0_0,
    EXPANDER_I2C0_1,
    EXPANDER_I2C0_2,
    EXPANDER_I2C0_3,
    EXPANDER_I2C1_0,
    EXPANDER_I2C1_1,
    EXPANDER_I2C1_2,
    EXPANDER_I2C1_3,

    INVALID,
};

enum class Data
{
    MCU,
    EXPANDER_I2C0_0,
    EXPANDER_I2C0_1,
    EXPANDER_I2C0_2,
    EXPANDER_I2C0_3,
    EXPANDER_I2C1_0,
    EXPANDER_I2C1_1,
    EXPANDER_I2C1_2,
    EXPANDER_I2C1_3,

    INVALID,
};
static constexpr uint16_t MASK = 0b000'1111'0000;
static constexpr uint16_t POS_SHIFT = 4;

namespace DataLength {
static constexpr std::size_t statusMsg = 3;
static constexpr std::size_t mcuConfigMsg = 7;
static constexpr std::size_t expanderConfigMsg = 6;
static constexpr std::size_t mcuDataMsg = 7;
static constexpr std::size_t expanderDataMsg = 3;
}
}

struct MessageID
{
    MessageType::Type type;
    std::variant<MessageSubtype::Status, MessageSubtype::Config, MessageSubtype::Data> subtype;
    NodeAddress::Address nodeAddress;

    uint16_t rawValue = 0;

    void parseRaw(uint16_t id);
};

using Payload = std::array<uint8_t, 8>;

struct Frame
{
    MessageID id = {};
    Payload payload = {};
};

[[nodiscard]] BSP::CAN_Ctrl::Filter
generateFilter(NodeAddress::Address nodeAddr);

void
scheduleToSend(Frame frame);
[[nodiscard]] CAN_Comm::Frame
getReceivedFrame();

[[nodiscard]] bool
sendStatus();

[[nodiscard]] static inline bool
isFrameAvailable()
{
    return !System::getQueueCanFrames().empty();
}
}
