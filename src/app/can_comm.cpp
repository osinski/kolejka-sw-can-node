/*
 * can_comm.cpp
 * Copyright (C) 2023 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include <cstdint>

#include "can_comm.hpp"

void
CAN_Comm::MessageID::parseRaw(uint16_t id)
{
    this->rawValue = id;

    uint32_t masked = (id & static_cast<uint32_t>(CAN_Comm::MessageType::MASK)) >>
                      static_cast<uint32_t>(CAN_Comm::MessageType::POS_SHIFT);
    if (masked >= static_cast<uint32_t>(CAN_Comm::MessageType::Type::INVALID)) {
        this->type = CAN_Comm::MessageType::Type::INVALID;
    } else {
        this->type = static_cast<CAN_Comm::MessageType::Type>(masked);
    }

    masked = (id & static_cast<uint32_t>(CAN_Comm::NodeAddress::MASK)) >>
             static_cast<uint32_t>(CAN_Comm::NodeAddress::POS_SHIFT);
    if (masked >= static_cast<uint32_t>(CAN_Comm::NodeAddress::Address::INVALID)) {
        this->nodeAddress = CAN_Comm::NodeAddress::Address::INVALID;
    } else {
        this->nodeAddress = static_cast<CAN_Comm::NodeAddress::Address>(masked);
    }

    masked = (id & static_cast<uint32_t>(CAN_Comm::MessageSubtype::MASK)) >>
             static_cast<uint32_t>(CAN_Comm::MessageSubtype::POS_SHIFT);
    if (this->type == CAN_Comm::MessageType::Type::STATUS) {
        if (masked >= static_cast<uint32_t>(CAN_Comm::MessageSubtype::Status::INVALID)) {
            this->subtype = CAN_Comm::MessageSubtype::Status::INVALID;
        } else {
            this->subtype = static_cast<CAN_Comm::MessageSubtype::Status>(masked);
        }
    } else if (this->type == CAN_Comm::MessageType::Type::IO_CONFIGURE) {
        if (masked >= static_cast<uint32_t>(CAN_Comm::MessageSubtype::Config::INVALID)) {
            this->subtype = CAN_Comm::MessageSubtype::Config::INVALID;
        } else {
            this->subtype = static_cast<CAN_Comm::MessageSubtype::Config>(masked);
        }
    } else if (this->type == CAN_Comm::MessageType::Type::INPUTS_REPORT ||
               this->type == CAN_Comm::MessageType::Type::OUTPUTS_SET) {
        if (masked >= static_cast<uint32_t>(CAN_Comm::MessageSubtype::Data::INVALID)) {
            this->subtype = CAN_Comm::MessageSubtype::Data::INVALID;
        } else {
            this->subtype = static_cast<CAN_Comm::MessageSubtype::Data>(masked);
        }
    } else {
        this->subtype = CAN_Comm::MessageSubtype::Status::INVALID;
    }
}

[[nodiscard]] BSP::CAN_Ctrl::Filter
CAN_Comm::generateFilter(CAN_Comm::NodeAddress::Address nodeAddr)
{
    // Acceptance Mask Bits - '0' means "match corresponding acceptance code"
    //                      - '1' means "ignore corresponding acceptance code"

    BSP::CAN_Ctrl::Filter filter{};

    if (nodeAddr == CAN_Comm::NodeAddress::Address::INVALID) [[unlikely]] {
        filter.acceptanceValue = 0xff'ff'ff'ff;
        filter.maskValue = 0xff'ff'ff'ff;
    } else {
        // NOTE: in MSCAN with standard 11-bit IDs, the values have to be left-aligned on 32-bit
        // TODO/PORTABILITY: make it more generic than just for KE06 MSCAN
        static constexpr uint32_t filterBitWidth = 32;
        static constexpr uint32_t usedIDBitWidth = 11;
        static constexpr uint32_t offset = filterBitWidth - usedIDBitWidth;

        filter.acceptanceValue = static_cast<uint32_t>(nodeAddr) << offset;
        filter.maskValue = ~(CAN_Comm::NodeAddress::MASK << offset);
    }

    return filter;
}

void
CAN_Comm::scheduleToSend(CAN_Comm::Frame frame)
{
    const BSP::CAN_Ctrl::Frame translatedFrame{
        .id = frame.id.rawValue,
        .data = frame.payload,
    };
}

[[nodiscard]] CAN_Comm::Frame
CAN_Comm::getReceivedFrame()
{
    auto& canqueue = System::getQueueCanFrames();
    const auto frame = canqueue.front();
    canqueue.pop();

    CAN_Comm::Frame processedFrame{};

    processedFrame.id.parseRaw(frame.id);
    processedFrame.payload = frame.data;

    return processedFrame;
}

[[nodiscard]] bool
CAN_Comm::sendStatus()
{

    return true;
}
