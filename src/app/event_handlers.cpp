/*
 * event_handlers.cpp
 * Copyright (C) 2024 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "event_handlers.hpp"

#include "helpers.hpp"
#include "io_expanders.hpp"
#include "system.hpp"

namespace {
void
handleCanFrameStatus(CAN_Comm::MessageSubtype::Status subtype, const CAN_Comm::Payload& payload);
void
handleCanFrameConfig(CAN_Comm::MessageSubtype::Config subtype, const CAN_Comm::Payload& payload);
void
handleCanFrameSetOutputs(CAN_Comm::MessageSubtype::Data subtype, const CAN_Comm::Payload& payload);
void
handleCanFrameInvalid();
}

void
EventHandle::canFrame(CAN_Comm::Frame frame)
{
    System::getShell().log<Shell::LogLevel::DBG>("{}", __func__);

    switch (frame.id.type) {
        case CAN_Comm::MessageType::Type::STATUS:
            handleCanFrameStatus(std::get<CAN_Comm::MessageSubtype::Status>(frame.id.subtype),
                                 frame.payload);
            break;
        case CAN_Comm::MessageType::Type::IO_CONFIGURE:
            handleCanFrameConfig(std::get<CAN_Comm::MessageSubtype::Config>(frame.id.subtype),
                                 frame.payload);
            break;
        case CAN_Comm::MessageType::Type::OUTPUTS_SET:
            handleCanFrameSetOutputs(std::get<CAN_Comm::MessageSubtype::Data>(frame.id.subtype),
                                     frame.payload);
            break;
        case CAN_Comm::MessageType::Type::INPUTS_REPORT:
            // this frame is node -> rpi, so normally we should never receive this
            // fallthrough
        case CAN_Comm::MessageType::Type::INVALID:
            handleCanFrameInvalid();
            break;
    }
}

void
EventHandle::ioExpanderChange(CAN_Comm::NodeAddress::Address addr)
{
    System::getShell().log<Shell::LogLevel::DBG>("{}", __func__);


}

void
EventHandle::ioLocalChange(CAN_Comm::NodeAddress::Address addr)
{
    System::getShell().log<Shell::LogLevel::DBG>("{}", __func__);


}

namespace {
void
handleCanFrameStatus(CAN_Comm::MessageSubtype::Status subtype, const CAN_Comm::Payload& payload)
{
    auto& shell = System::getShell();
    shell.log<Shell::LogLevel::DBG>("{}", __func__);
    switch (subtype) {
        case CAN_Comm::MessageSubtype::Status::STATUS:
            break;
        case CAN_Comm::MessageSubtype::Status::INVALID:
            handleCanFrameInvalid();
            break;
    }
}

void
handleCanFrameConfig(CAN_Comm::MessageSubtype::Config subtype, const CAN_Comm::Payload& payload)
{
    auto& shell = System::getShell();
    shell.log<Shell::LogLevel::DBG>("{}", __func__);
    switch (subtype) {
        case CAN_Comm::MessageSubtype::Config::MCU_IO_USED:
            break;
        case CAN_Comm::MessageSubtype::Config::MCU_IO_DIRECTIONS:
            break;
        case CAN_Comm::MessageSubtype::Config::EXPANDER_I2C0_0:
        case CAN_Comm::MessageSubtype::Config::EXPANDER_I2C0_1:
        case CAN_Comm::MessageSubtype::Config::EXPANDER_I2C0_2:
        case CAN_Comm::MessageSubtype::Config::EXPANDER_I2C0_3:
        case CAN_Comm::MessageSubtype::Config::EXPANDER_I2C1_0:
        case CAN_Comm::MessageSubtype::Config::EXPANDER_I2C1_1:
        case CAN_Comm::MessageSubtype::Config::EXPANDER_I2C1_2:
        case CAN_Comm::MessageSubtype::Config::EXPANDER_I2C1_3:
            IO::Expanders::configure(
              Helper::getExpanderAddrFromSubtype(subtype),
              IO::Expanders::ConfigData{
                payload[0], payload[1], payload[2], payload[3], payload[4], payload[5] });
            break;
        case CAN_Comm::MessageSubtype::Config::INVALID:
            handleCanFrameInvalid();
            break;
    }
}

void
handleCanFrameSetOutputs(CAN_Comm::MessageSubtype::Data subtype, const CAN_Comm::Payload& payload)
{
    auto& shell = System::getShell();
    shell.log<Shell::LogLevel::DBG>("{}", __func__);
    switch (subtype) {
        case CAN_Comm::MessageSubtype::Data::MCU:
            break;
        case CAN_Comm::MessageSubtype::Data::EXPANDER_I2C0_0:
        case CAN_Comm::MessageSubtype::Data::EXPANDER_I2C0_1:
        case CAN_Comm::MessageSubtype::Data::EXPANDER_I2C0_2:
        case CAN_Comm::MessageSubtype::Data::EXPANDER_I2C0_3:
        case CAN_Comm::MessageSubtype::Data::EXPANDER_I2C1_0:
        case CAN_Comm::MessageSubtype::Data::EXPANDER_I2C1_1:
        case CAN_Comm::MessageSubtype::Data::EXPANDER_I2C1_2:
        case CAN_Comm::MessageSubtype::Data::EXPANDER_I2C1_3:
            IO::Expanders::setOutputs(
              Helper::getExpanderAddrFromSubtype(subtype),
              IO::Expanders::PortsData{ payload[0], payload[1], payload[2] });
            break;
        case CAN_Comm::MessageSubtype::Data::INVALID:
            handleCanFrameInvalid();
            break;
    }
}

void
handleCanFrameInvalid()
{
    auto& shell = System::getShell();
    shell.log<Shell::LogLevel::ERR>("Odebrano niepoprawną/nieznaną ramkę");
}
}
