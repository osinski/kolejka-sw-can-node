/*
 * helpers.hpp
 * Copyright (C) 2024 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#pragma once

#include <optional>

#include "custom_formatters.hpp"
#include "board.hpp"
#include "can_comm.hpp"
#include "io_expanders.hpp"
#include "system.hpp"

namespace Helper {
using TestExpander = std::pair<System::I2C::Driver*, PCAL6524::Address>;

static inline CAN_Comm::NodeAddress::Address
getNodeAddrFromBoardCfg(Board::Configuration boardCfg)
{
    switch (boardCfg) {
        case Board::Configuration::WORKING_NODE_1:
            return CAN_Comm::NodeAddress::Address::NODE_1;
        case Board::Configuration::WORKING_NODE_2:
            return CAN_Comm::NodeAddress::Address::NODE_2;
        case Board::Configuration::WORKING_NODE_3:
            return CAN_Comm::NodeAddress::Address::NODE_3;
        case Board::Configuration::WORKING_NODE_4:
            return CAN_Comm::NodeAddress::Address::NODE_4;
        case Board::Configuration::WORKING_NODE_5:
            return CAN_Comm::NodeAddress::Address::NODE_5;
        case Board::Configuration::WORKING_NODE_6:
            return CAN_Comm::NodeAddress::Address::NODE_6;
        case Board::Configuration::WORKING_NODE_7:
            return CAN_Comm::NodeAddress::Address::NODE_7;
        case Board::Configuration::WORKING_NODE_8:
            return CAN_Comm::NodeAddress::Address::NODE_8;
        case Board::Configuration::WORKING_NODE_9:
            return CAN_Comm::NodeAddress::Address::NODE_9;
        case Board::Configuration::SELF_TEST:
        case Board::Configuration::RESERVED:
            return CAN_Comm::NodeAddress::Address::INVALID;
    }
    // default case
    return CAN_Comm::NodeAddress::Address::INVALID;
}

template<typename SubType>
IO::Expanders::Addr
getExpanderAddrFromSubtype(SubType subtype)
{
    switch (subtype) {
        case SubType::EXPANDER_I2C0_0:
            return IO::Expanders::Addr{ 0, PCAL6524::Address::GND };
        case SubType::EXPANDER_I2C0_1:
            return IO::Expanders::Addr{ 0, PCAL6524::Address::VDD };
        case SubType::EXPANDER_I2C0_2:
            return IO::Expanders::Addr{ 0, PCAL6524::Address::SDA };
        case SubType::EXPANDER_I2C0_3:
            return IO::Expanders::Addr{ 0, PCAL6524::Address::SCL };
        case SubType::EXPANDER_I2C1_0:
            return IO::Expanders::Addr{ 1, PCAL6524::Address::GND };
        case SubType::EXPANDER_I2C1_1:
            return IO::Expanders::Addr{ 1, PCAL6524::Address::VDD };
        case SubType::EXPANDER_I2C1_2:
            return IO::Expanders::Addr{ 1, PCAL6524::Address::SDA };
        case SubType::EXPANDER_I2C1_3:
            return IO::Expanders::Addr{ 1, PCAL6524::Address::SCL };

        // if any other value is passed to this function then it is used wrong
        default:
            return IO::Expanders::Addr{ 0, PCAL6524::Address::GND };
    }
}

static inline std::optional<TestExpander>
getTestExpander(const IO::Expanders::Existing& existing)
{
    auto& i2cDrivers = System::getDriversI2C();
    auto& shell = System::getShell();
    System::I2C::Driver* testI2cBus = &i2cDrivers[0];
    PCAL6524::Address testExpander = PCAL6524::Address::GND;
    bool found = false;

    for (size_t i = 0; i < existing.size(); i++) {
        for (size_t j = 0; j < existing[i].size(); j++) {
            if (existing[i].test(j)) {
                testI2cBus = &i2cDrivers[i];
                testExpander = IO::Expanders::idxToAddr(j).value();
                shell.println("{}Znaleziono Expander {} na magistrali I2C{}",
                              ANSI_EscapeCodes::Colors::Foreground::green,
                              testExpander,
                              i);
                found = true;
            } else {
                // TODO: get rid of addressToStr and write fmt custom formatter?
                shell.println("{}Brak Expandera {} na magistrali I2C{}",
                              ANSI_EscapeCodes::Colors::Foreground::yellow,
                              IO::Expanders::idxToAddr(j).value(),
                              i);
            }
        }
    }
    shell.println(ANSI_EscapeCodes::reset);

    if (!found) {
        return std::nullopt;
    }
    return std::make_pair(testI2cBus, testExpander);
}
}
