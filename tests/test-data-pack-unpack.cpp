/*
 * test-data-pack-unpack.cpp
 * Copyright (C) 2023 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "CppUTest/TestHarness.h"

#include "data.hpp"

TEST_GROUP(DataTestGroup){ void setup(){} void teardown(){} };

TEST(DataTestGroup, PackTest)
{
    GpioMCUBitsArray gpioPortsUsedPins{ 0xFF'00'C3'00, 0xFF'FF'FF'FC, 0x00'00'00'1F };
    GpioMCUBitsArray testInputs{ 0x00'00'00'00, 0x00'00'00'00, 0x00'00'00'00 };
    PackedBytesArray packed = Data::pack(testInputs, gpioPortsUsedPins);

    BYTES_EQUAL(0x00, packed[0]);
    BYTES_EQUAL(0x00, packed[1]);
    BYTES_EQUAL(0x00, packed[2]);
    BYTES_EQUAL(0x00, packed[3]);
    BYTES_EQUAL(0x00, packed[4]);
    BYTES_EQUAL(0x00, packed[5]);
    BYTES_EQUAL(0x00, packed[6]);

    testInputs = { 0xFF'FF'FF'FF, 0xFF'FF'FF'FF, 0x00'00'00'FF };
    packed = Data::pack(testInputs, gpioPortsUsedPins);

    BYTES_EQUAL(0x0F, packed[0]);
    BYTES_EQUAL(0xFF, packed[1]);
    BYTES_EQUAL(0x3F, packed[2]);
    BYTES_EQUAL(0xFF, packed[3]);
    BYTES_EQUAL(0xFF, packed[4]);
    BYTES_EQUAL(0xFF, packed[5]);
    BYTES_EQUAL(0x1F, packed[6]);

    testInputs = { 0xAA'00'81'00, 0x77'88'00'14, 0x00'00'00'03 };
    packed = Data::pack(testInputs, gpioPortsUsedPins);

    BYTES_EQUAL(0x09, packed[0]);
    BYTES_EQUAL(0xAA, packed[1]);
    BYTES_EQUAL(0x05, packed[2]);
    BYTES_EQUAL(0x00, packed[3]);
    BYTES_EQUAL(0x88, packed[4]);
    BYTES_EQUAL(0x77, packed[5]);
    BYTES_EQUAL(0x03, packed[6]);

    testInputs = { 0xDE'00'AD'00, 0xBE'EF'08'F0, 0x00'00'00'08 };
    packed = Data::pack(testInputs, gpioPortsUsedPins);

    BYTES_EQUAL(0x09, packed[0]);
    BYTES_EQUAL(0xDE, packed[1]);
    BYTES_EQUAL(0x3C, packed[2]);
    BYTES_EQUAL(0x08, packed[3]);
    BYTES_EQUAL(0xEF, packed[4]);
    BYTES_EQUAL(0xBE, packed[5]);
    BYTES_EQUAL(0x08, packed[6]);
}

TEST(DataTestGroup, UnpackTest)
{
    GpioMCUBitsArray gpioPortsUsedPins{ 0xFF'00'C3'00, 0xFF'FF'FF'FC, 0x00'00'00'1F };
    PackedBytesArray testInputs{ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
    GpioMCUBitsArray unpacked = Data::unpack(testInputs, gpioPortsUsedPins);

    LONGS_EQUAL(0x00'00'00'00, unpacked[0].to_ulong());
    LONGS_EQUAL(0x00'00'00'00, unpacked[1].to_ulong());
    LONGS_EQUAL(0x00'00'00'00, unpacked[2].to_ulong());

    testInputs = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
    unpacked = Data::unpack(testInputs, gpioPortsUsedPins);

    LONGS_EQUAL(0xFF'00'C3'00, unpacked[0].to_ulong());
    LONGS_EQUAL(0xFF'FF'FF'FC, unpacked[1].to_ulong());
    LONGS_EQUAL(0x00'00'00'1F, unpacked[2].to_ulong());

    testInputs = { 0x03, 0x55, 0x25, 0x11, 0x00, 0xAA, 0x13 };
    unpacked = Data::unpack(testInputs, gpioPortsUsedPins);

    LONGS_EQUAL(0x55'00'03'00, unpacked[0].to_ulong());
    LONGS_EQUAL(0xAA'00'11'94, unpacked[1].to_ulong());
    LONGS_EQUAL(0x00'00'00'13, unpacked[2].to_ulong());

    testInputs = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFA, 0xDE, 0x88 };
    unpacked = Data::unpack(testInputs, gpioPortsUsedPins);

    LONGS_EQUAL(0xAD'00'C2'00, unpacked[0].to_ulong());
    LONGS_EQUAL(0xDE'FA'EF'F8, unpacked[1].to_ulong());
    LONGS_EQUAL(0x00'00'00'08, unpacked[2].to_ulong());
}

// TEST(DataTestGroup, CombinedTest)
//{
//     GpioBitsArray gpioPortsUsedPins{ 0xFF'00'C3'00, 0xFF'FF'FF'FC, 0x00'00'00'1F };
//     GpioBitsArray testInputsToPack{ 0x00'00'00'00, 0x00'00'00'00, 0x00'00'00'00 };
//     PackedBytesArray packed = Data::pack(testInputsToPack, gpioPortsUsedPins);
//     GpioBitsArray unpacked = Data::unpack(packed, gpioPortsUsedPins);
//
//     for (size_t i = 0; i < testInputsToPack.size(); ++i) {
//         CHECK_EQUAL(testInputsToPack[i].to_ulong(), unpacked[i].to_ulong());
//     }
// }
