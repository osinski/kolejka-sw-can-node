# CAN-Node Firmware

## How to..

### How to build

1. First create and go to **build** folder (``mkdir build && cd build``
   from project root).

1. Type ``cmake -DCMAKE_TOOLCHAIN_FILE=../cmake/cortex-m0plus.cmake 
   -DCMAKE_BUILD_TYPE=Debug ..`` to make cmake generate build system based on
   makefiles. 

   You can also:
   - change ``Debug`` to ``Release``
   - add ``-GNinja`` to use `ninja` instead of `make` as generator,
   - add ``--graphviz=<some_name>.dot`` to generate graph showing dependencies
     between targets in the project. Multiple dot files are generated, each
     shows on what other targets the target specified in file name depends
     and which targets depend on that target. Use ``dot -Tsvg -o <svg_name>.svg
     <dot_file_name>`` to convert `dot` file to `svg`,

1. Use ``ccmake ..`` (from **build** folder)
   to see and change different cached values and options, such as:
   - **GENERATE_MAP**
   - **BUILD_DOCS** (not yet implemented)
   - **BUILD_TESTS** (not yet implemented)
   - **ENABLE_WATCHDOG** (not yet implemented)
   - **DEBUG_AFTER_BUILD**
   - **FLASH_AFTER_BUILD**

1. Type ``cmake --build .`` without leaving the **build** folder to build the
   project.


### How to flash the board

Use ``flash.sh`` helper script located in ``scripts`` folder, 
i.e. ``./scripts/flash.sh``.


### How to build and run tests

**NOTE:** make sure you have *CppUTest* framework installed and it's
discoverable by ``pkg-config``

1. First create and go to **build-tests** folder
   (``mkdir build-tests && cd build-tests`` from project root).

1. Type ``cmake ../tests`` to make cmake generate build system for tests based 
   on makefiles. 

1. Type ``cmake --build .`` without leaving the **build-tests** folder to build
   executable with tests.

1. Run ``ctest`` to run tests.



## Project description



